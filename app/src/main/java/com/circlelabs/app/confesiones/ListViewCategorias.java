package com.circlelabs.app.confesiones;


/**
 * Created by Fabian on 24/06/2016.
 */
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class ListViewCategorias extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> idCategoria;
    private final ArrayList<String> nombreCategoria;
    private final ArrayList<String> foto;

    public ListViewCategorias(
            Activity context,
            ArrayList<String> idCategoria,
            ArrayList<String> nombreCategoria,
            ArrayList<String> foto) {
        super(context, R.layout.list_view_confesiones,idCategoria);
        this.context = context;
        this.idCategoria = idCategoria;
        this.nombreCategoria = nombreCategoria;
        this.foto = foto;

    }

    TextView tvTitulo,tvNombreGrupo,expandable_text,tvCantidadComentario,tvCantidadLike,tvFechaPublicacion;
    ImageView ivFotoGrupo;
    String url;
    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        String longUrl = "http://www.google.com";

        LayoutInflater inflater = context.getLayoutInflater();

        final View rowView= inflater.inflate(R.layout.list_view_categorias, null, true);

        tvNombreGrupo = (TextView) rowView.findViewById(R.id.tvNombreGrupo);
        ivFotoGrupo   = (ImageView) rowView.findViewById(R.id.ivFotoGrupo);
        tvNombreGrupo.setText(nombreCategoria.get(position));

        Drawable man = context.getResources().getDrawable(R.drawable.lace);

        url = "http://www.circlelabs.cl/app/confeciones/images/grupo/" + foto.get(position) + ".jpeg";
        Glide
                .with( getContext() )
                .load( url )
                .error(man)
                .thumbnail( 0.05f )
                .fitCenter()
                .bitmapTransform(new CropCircleTransformation(context))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into( ivFotoGrupo );

        return rowView;
    }

    public static String shorten(String longUrl) {

        if (longUrl == null) {
            return longUrl;
        }
        StringBuilder sb = null;
        String line = null;
        String urlStr = longUrl;

        try {
            URL url = new URL("http://goo.gl/api/url");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("User-Agent", "toolbar");

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write("url=" + URLEncoder.encode(urlStr, "UTF-8"));
            writer.close();

            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            sb = new StringBuilder();
            while ((line = rd.readLine()) != null) {
                sb.append(line + '\n');
            }

            String json = sb.toString();
            //It extracts easily...
            return json.substring(json.indexOf("http"), json.indexOf("\"", json.indexOf("http")));
        } catch (MalformedURLException e) {
            return longUrl;
        } catch (IOException e) {
            return longUrl;
        }
    }
}