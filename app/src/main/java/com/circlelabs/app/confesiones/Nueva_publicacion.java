package com.circlelabs.app.confesiones;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class Nueva_publicacion extends AppCompatActivity {


    RequestQueue requestQueue;
    ProgressDialog progressDialog;
    Button btnAddPublicacion;
    TextView tvContenido;
    TextView tvTitulo;
    Spinner acGrupos;
    ArrayList grupo   = new ArrayList<String>();
    ArrayList idGrupo = new ArrayList<String>();

    String itemSeleccionado;
    boolean existeIdUsuario = true;

    SessionManager manager;
    String sesionUsuario;

    VideoView videoView1,videoView2,videoView3,videoView4,videoView5,videoView6,videoView7;
    VideoView gifSeleccionado;
    String link1,link2,link3,link4,link5,link6,link7;
    String width1,width2,width3,width4,width5,width6,width7;
    EditText etBuscarGif;
    HorizontalScrollView horizontalScrollViewGif;
    LinearLayout contenedorGifSeleccionado;
    String linkGifSeleccionado = null;
    String widthGifSeleccionado = null;

    long startClickTime;
    final int MAX_CLICK_DURATION = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_publicacion);

        manager = new SessionManager();
        sesionUsuario = manager.getPreferences(Nueva_publicacion.this, "sesionUsuario");

        if(sesionUsuario.isEmpty()){
            Intent intent = new Intent(this, Login.class);
            //String idCategoria = String.valueOf(idCategoria1);
            //intent.putExtra(EXTRA_MESSAGE, idCategoria);
            //intent.putExtra("NombreGrupoParam", item.getTitle());
            startActivity(intent);
        }


        //cambiar nombre del action bar
        getSupportActionBar().setTitle("Nueva confesión");
        getSupportActionBar().setElevation(0);
        //Habilitar la flechita para atras
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable( new ColorDrawable( Color.parseColor("#322f60") ) );

        btnAddPublicacion = (Button) findViewById(R.id.btnAddPublicacion);
        tvTitulo          = (TextView) findViewById(R.id.tvTitulo);
        tvContenido       = (TextView) findViewById(R.id.tvContenido);
        acGrupos          = (Spinner) findViewById(R.id.acGrupos);

        Drawable tiempo = getResources().getDrawable(R.drawable.relojarena);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminateDrawable(tiempo);
        progressDialog.setMessage("Subiendo publicación");

        getCategorias();

        /***/
        contenedorGifSeleccionado = (LinearLayout) findViewById(R.id.contenedorGifSeleccionado);
        gifSeleccionado = (VideoView) findViewById(R.id.gifSeleccionado);
        horizontalScrollViewGif = (HorizontalScrollView) findViewById(R.id.horizontalScrollViewGif);
        etBuscarGif = (EditText) findViewById(R.id.etBuscarGif);
        videoView1 = (VideoView) findViewById(R.id.vidGiphy1);
        videoView2 = (VideoView) findViewById(R.id.vidGiphy2);
        videoView3 = (VideoView) findViewById(R.id.vidGiphy3);
        videoView4 = (VideoView) findViewById(R.id.vidGiphy4);
        videoView5 = (VideoView) findViewById(R.id.vidGiphy5);
        videoView6 = (VideoView) findViewById(R.id.vidGiphy6);
        videoView7 = (VideoView) findViewById(R.id.vidGiphy7);

        gifSeleccionado.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

        horizontalScrollViewGif.postDelayed(new Runnable() {
            public void run() {
                horizontalScrollViewGif.fullScroll(HorizontalScrollView.FOCUS_LEFT);
            }
        }, 100L);

        etBuscarGif.setInputType(InputType.TYPE_CLASS_TEXT);
        etBuscarGif.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    System.out.println( etBuscarGif.getText().toString() );

                    //Llamar a buscar el gif
                    horizontalScrollViewGif.setVisibility(View.VISIBLE);
                    getGiphy(etBuscarGif.getText().toString());
                    horizontalScrollViewGif.fullScroll(HorizontalScrollView.FOCUS_LEFT);

                    // Check if no view has focus:
                    View view = Nueva_publicacion.this.getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    return true;
                }
                return false;
            }
        });

        videoView1.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();

                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            //click event has occurred
                            //System.out.println(link1);
                            System.out.println("DOWN EVENT");

                            linkGifSeleccionado = link1;
                            widthGifSeleccionado = width1;
                            Uri video = Uri.parse(link1);
                            gifSeleccionado.getLayoutParams().height = Integer.parseInt(width1);
                            gifSeleccionado.setVideoURI(video);
                            gifSeleccionado.start();
                            contenedorGifSeleccionado.setVisibility(View.VISIBLE);
                            horizontalScrollViewGif.setVisibility(View.GONE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;
            }
        });

        videoView2.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();

                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        System.out.println("MOVE EVENT");
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            //click event has occurred
                            //System.out.println(link1);
                            linkGifSeleccionado = link2;
                            widthGifSeleccionado = width2;
                            Uri video = Uri.parse(link2);
                            gifSeleccionado.getLayoutParams().height = Integer.parseInt(width2);
                            gifSeleccionado.setVideoURI(video);
                            gifSeleccionado.start();
                            contenedorGifSeleccionado.setVisibility(View.VISIBLE);
                            horizontalScrollViewGif.setVisibility(View.GONE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;



            }
        });

        videoView3.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        System.out.println("MOVE EVENT");
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            //click event has occurred
                            linkGifSeleccionado = link3;
                            widthGifSeleccionado = width3;
                            Uri video = Uri.parse(link3);
                            gifSeleccionado.getLayoutParams().height = Integer.parseInt(width3);
                            gifSeleccionado.setVideoURI(video);
                            gifSeleccionado.start();
                            contenedorGifSeleccionado.setVisibility(View.VISIBLE);
                            horizontalScrollViewGif.setVisibility(View.GONE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;
            }
        });

        videoView4.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();
                        //System.out.println(link4);
                        System.out.println("DOWN EVENT");
                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        System.out.println("MOVE EVENT");
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            //click event has occurred
                            linkGifSeleccionado = link4;
                            widthGifSeleccionado = width4;
                            Uri video = Uri.parse(link4);
                            gifSeleccionado.getLayoutParams().height = Integer.parseInt(width4);
                            gifSeleccionado.setVideoURI(video);
                            gifSeleccionado.start();
                            contenedorGifSeleccionado.setVisibility(View.VISIBLE);
                            horizontalScrollViewGif.setVisibility(View.GONE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;
            }
        });

        videoView5.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        System.out.println("MOVE EVENT");
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            //click event has occurred
                            linkGifSeleccionado = link5;
                            widthGifSeleccionado = width5;
                            Uri video = Uri.parse(link5);
                            gifSeleccionado.getLayoutParams().height = Integer.parseInt(width5);
                            gifSeleccionado.setVideoURI(video);
                            gifSeleccionado.start();
                            contenedorGifSeleccionado.setVisibility(View.VISIBLE);
                            horizontalScrollViewGif.setVisibility(View.GONE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;

            }
        });

        videoView6.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView6.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        System.out.println("MOVE EVENT");
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            //click event has occurred
                            linkGifSeleccionado = link6;
                            widthGifSeleccionado = width6;
                            Uri video = Uri.parse(link6);
                            gifSeleccionado.getLayoutParams().height = Integer.parseInt(width6);
                            gifSeleccionado.setVideoURI(video);
                            gifSeleccionado.start();
                            contenedorGifSeleccionado.setVisibility(View.VISIBLE);
                            horizontalScrollViewGif.setVisibility(View.GONE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;
            }
        });

        videoView7.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView7.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();

                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        System.out.println("MOVE EVENT");
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            //click event has occurred
                            linkGifSeleccionado = link7;
                            widthGifSeleccionado = width7;
                            Uri video = Uri.parse(link7);
                            gifSeleccionado.getLayoutParams().height = Integer.parseInt(width7);
                            gifSeleccionado.setVideoURI(video);
                            gifSeleccionado.start();
                            contenedorGifSeleccionado.setVisibility(View.VISIBLE);
                            horizontalScrollViewGif.setVisibility(View.GONE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;
            }
        });
        /***/

        acGrupos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                System.out.println("jkalsdjalsk");
            }
        });

        btnAddPublicacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String indexGrupoSeleccionado = acGrupos.getText().toString();
                //String idGrupoSeleccinado = idGrupo.get( grupo.indexOf(indexGrupoSeleccionado)).toString();

                String indexGrupoSeleccionado =acGrupos.getSelectedItem().toString();
                String idGrupoSeleccinado = idGrupo.get( grupo.indexOf(indexGrupoSeleccionado)).toString();

                System.out.println("INDEX SELECCC " + indexGrupoSeleccionado + " /////// " + idGrupoSeleccinado);
                String url = "http://www.circlelabs.cl/app/confeciones/addPublicacion.php";
                requestQueue = Volley.newRequestQueue(getApplicationContext());
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("contenido", tvContenido.getText().toString());
                params.put("id_categoria", idGrupoSeleccinado);
                params.put("titulo",tvTitulo.getText().toString());
                params.put("id_usuario", sesionUsuario);
                params.put("link", linkGifSeleccionado);
                params.put("width", widthGifSeleccionado);



                progressDialog.show();

                final JSONObject parameters = new JSONObject(params);


                JsonObjectRequest json2 = new JsonObjectRequest
                        (url, parameters,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response) {

                                        try {
                                            progressDialog.hide();
                                            System.out.println("RESPONSE");
                                            System.out.println(response);
                                            JSONObject objeto = response.getJSONObject("publicacion");
                                            System.out.println(objeto.getString("resultado"));

                                            if(objeto.getString("resultado").equals("ok")){
                                                //Toast.makeText("Publicacion añadida correctamente", Toast.LENGTH_LONG);

                                                Intent nextPage = new Intent(getApplicationContext(),MainActivity.class);
                                                startActivity(nextPage);

                                            }
                                            else{

                                            }






                                        } catch (Exception e) {
                                            //loading.dismiss();
                                            Log.e("ERROR", "ERROR");
                                            e.printStackTrace();
                                            System.out.println(e.getMessage());
                                            progressDialog.hide();
                                        }

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        // TODO Auto-generated method stub
                                        Log.e("VOLLEYeror", error.toString());
                                        requestQueue.stop();
                                       // progressDialog.hide();
                                    }
                                }
                        );
                json2.setRetryPolicy(new DefaultRetryPolicy(
                        5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
                requestQueue.add(json2);

                System.out.println("BOTON QWL");
            }
        });
    }

    public void getCategorias(){
        String url = "http://www.circlelabs.cl/app/confeciones/getCategorias.php";

        requestQueue = Volley.newRequestQueue(this);
        HashMap<String, String> params = new HashMap<String, String>();
        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    //System.out.println("RESPONSE");
                                    //System.out.println(response);
                                    JSONArray arrayResponse = response.getJSONArray("grupos");
                                    System.out.println(arrayResponse);

                                    for(int i=0; i<arrayResponse.length(); i++){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);
                                        grupo.add(objeto.getString("nombre"));
                                        idGrupo.add(objeto.getString("idGrupo"));
                                    }

                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                            (Nueva_publicacion.this,android.R.layout.select_dialog_item, grupo);


                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                    acGrupos.setAdapter(adapter);



                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);
    }

    public void getGiphy(String busqeda) {
        final RequestQueue rq = Volley.newRequestQueue(getApplicationContext());
        String link = "https://api.giphy.com/v1/gifs/search?api_key=befb8defdf2b4728a9d211429bab5a81&q="+busqeda+"&limit=10&offset=0&rating=G&lang=es";
        HashMap<String, String> params2 = new HashMap<String, String>();

        final JSONObject parameters = new JSONObject(params2);
        JsonObjectRequest json2 = new JsonObjectRequest
                (link, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response7) {
                                try {
                                    //System.out.println(response7);
                                    JSONArray arrayResponse = response7.getJSONArray("data");

                                    for(int i=0; i<arrayResponse.length(); i++){
                                        //if(i==0){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);

                                        JSONObject objeto2 = objeto.getJSONObject("images");
                                        //System.out.println( objeto2.getString("downsized_small") );
                                        //System.out.println( objeto2.getString("original") );

                                        //JSONObject objeto3 = objeto2.getJSONObject("downsized_small");
                                        JSONObject objeto3 = objeto2.getJSONObject("fixed_width");
                                        System.out.println( objeto3.getString("mp4") );

                                            /*JSONObject objeto4 = objeto2.getJSONObject("original");
                                            System.out.println( objeto4.getString("mp4") );*/

                                        System.out.println("------------------------");

                                        String LINK = objeto3.getString("mp4").toString();
                                        String WIDTH = objeto3.getString("width").toString();

                                            /*MediaController mc = new MediaController(getApplicationContext());
                                            mc.setAnchorView(videoView);
                                            mc.setMediaPlayer(videoView);*/
                                        Uri video = Uri.parse(LINK);
                                        //videoView.setMediaController(mc);

                                        if(i == 0){
                                            videoView1.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView1.setVideoURI(video);
                                            videoView1.start();
                                            link1 = LINK;
                                            width1 = WIDTH;
                                        }

                                        if(i == 2){
                                            videoView2.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView2.setVideoURI(video);
                                            videoView2.start();
                                            link2 = LINK;
                                            width2 = WIDTH;
                                        }


                                        if(i == 3){
                                            videoView3.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView3.setVideoURI(video);
                                            videoView3.start();
                                            link3 = LINK;
                                            width3 = WIDTH;
                                        }

                                        if(i == 4){
                                            videoView4.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView4.setVideoURI(video);
                                            videoView4.start();
                                            link4 = LINK;
                                            width4 = WIDTH;
                                        }

                                        if(i == 5){
                                            videoView5.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView5.setVideoURI(video);
                                            videoView5.start();
                                            link5 = LINK;
                                            width5 = WIDTH;
                                        }

                                        if(i == 6){
                                            videoView6.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView6.setVideoURI(video);
                                            videoView6.start();
                                            link6 = LINK;
                                            width6 = WIDTH;
                                        }

                                        if(i == 7){
                                            videoView7.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView7.setVideoURI(video);
                                            videoView7.start();
                                            link7 = LINK;
                                            width7 = WIDTH;
                                        }



                                        //}



                                        /*arrayContenido.add(objeto.getString("contenido"));
                                        arrayComentario.add(objeto.getString("cantidad_comentarios"));
                                        arrayLike.add(objeto.getString("likes"));
                                        arrayHoraPublicacion.add(objeto.getString("fecha_publicacion"));
                                        arrayTitulo.add(objeto.getString("titulo"));
                                        arrayIdPublicacion.add(objeto.getString("id_publicacion"));*/
                                    }

                                } catch (Exception e) {
                                    Log.e("ERROR","ERROR");
                                    e.printStackTrace();
                                    System.out.println( e.getMessage() );
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                System.out.println("AYURAAAAA!!!!!!!!!!");
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror",error.toString());

                                rq.stop();
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        rq.add(json2);
    }
}
