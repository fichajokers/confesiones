package com.circlelabs.app.confesiones;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.firebase.iid.FirebaseInstanceId;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.HashMap;

import jp.wasabeef.glide.transformations.BlurTransformation;
import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    public static final String EXTRA_MESSAGE = "";

    RequestQueue requestQueue;
    ProgressDialog progressDialog,cancelDialog;
    String idCategoria1;
    String idCategoria2;
    String idCategoria3;

    ArrayList arrayIdPublicacion = new ArrayList<String>();
    ArrayList arrayTitulo = new ArrayList<String>();
    ArrayList arrayNombreGrupo = new ArrayList<String>();
    ArrayList arrayComentario = new ArrayList<String>();
    ArrayList arrayLike = new ArrayList<String>();
    ArrayList arrayHoraPublicacion = new ArrayList<String>();
    ArrayList arrayContenido = new ArrayList<String>();
    ArrayList arrayLink = new ArrayList<String>();
    ArrayList arrayWidth = new ArrayList<String>();

    ArrayAdapter arrayAdapterConfesiones;

    ListView listViewConfesiones;

    SessionManager manager;
    SwipeRefreshLayout swipeContainer;

    ImageView imagePerfilUsuario;

    String rutaFotoPerfil,nombreUsuario, sesionUsuario;

    TextView tvNombreUsuario;

    String url,fcm,guardarFcm;

    RelativeLayout relativeNuevaConfesion;

    Button btnNuevasConfesiones;

    Integer pagina = 20;

    VideoView videoView1,videoView2,videoView3,videoView4,videoView5,videoView6,videoView7;
    String link1,link2,link3,link4,link5,link6,link7;
    String width1,width2,width3,width4,width5,width6,width7;
    EditText etBuscarGif;
    HorizontalScrollView horizontalScrollViewGif;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
        getSupportActionBar().setBackgroundDrawable( new ColorDrawable( Color.parseColor("#322f60") ) );

        relativeNuevaConfesion = (RelativeLayout) findViewById(R.id.containerNuevasConfesiones);
        btnNuevasConfesiones = (Button) findViewById(R.id.recargarConfesiones);
        listViewConfesiones = (ListView) findViewById(R.id.contenedorListViewConfesiones);

        /*listViewConfesiones.setOnScrollListener(new AbsListView.OnScrollListener() {
            int currentFirstVisibleItem,currentVisibleItemCount,currentScrollState,total;

            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                currentFirstVisibleItem = firstVisibleItem;
                currentVisibleItemCount = visibleItemCount;
                total                   = totalItemCount;
            }

            public void onScrollStateChanged(AbsListView view, int scrollState) {
                currentScrollState = scrollState;
                this.isScrollCompleted();
            }

            private void isScrollCompleted() {
                if ( (currentFirstVisibleItem + currentVisibleItemCount == total) && currentScrollState == SCROLL_STATE_IDLE) {
                    /*** In this way I detect if there's been a scroll which has completed ***/
                    /*** do the work for load more date! ***/
                    /*if(!isLoading){
                        isLoading = true;
                        loadMoreData();
                    }
                    System.out.println("LLEGAMOS AL FINAL, PAGINA: " + pagina);

                    arrayIdPublicacion   = new ArrayList<String>();
                    arrayTitulo          = new ArrayList<String>();
                    arrayNombreGrupo     = new ArrayList<String>();
                    arrayComentario      = new ArrayList<String>();
                    arrayLike            = new ArrayList<String>();
                    arrayHoraPublicacion = new ArrayList<String>();
                    arrayContenido       = new ArrayList<String>();
                    getConfesionesPaginadas(pagina.toString());
                    pagina+=10;
                }
            }
        });*/

        btnNuevasConfesiones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayIdPublicacion   = new ArrayList<String>();
                arrayTitulo          = new ArrayList<String>();
                arrayNombreGrupo     = new ArrayList<String>();
                arrayComentario      = new ArrayList<String>();
                arrayLike            = new ArrayList<String>();
                arrayHoraPublicacion = new ArrayList<String>();
                arrayContenido       = new ArrayList<String>();
                arrayLink            = new ArrayList<String>();
                arrayWidth           = new ArrayList<String>();
                reloadNuevasConfesiones();
            }
        });


        System.out.println("CREATE");
        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new Vie(String) item.getTitle()w.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.plus);
        toolbar.setOverflowIcon(drawable);

        requestQueue = Volley.newRequestQueue(getApplicationContext());

        manager = new SessionManager();
        rutaFotoPerfil = manager.getPreferences(MainActivity.this,"sesionFotoUsuario");
        nombreUsuario  = manager.getPreferences(MainActivity.this,"sesionNombreUsuario");
        sesionUsuario = manager.getPreferences(MainActivity.this, "sesionUsuario");

        manager.setPreferences(this,"FCM", FirebaseInstanceId.getInstance().getToken() );
        fcm = manager.getPreferences(MainActivity.this, "FCM");
        guardarFcm = manager.getPreferences(MainActivity.this, "guardarFCM");

        if(  !sesionUsuario.equals("") && !guardarFcm.equals("true") ){
            manager.setPreferences(this,"guardarFCM", "true" );
            System.out.println("GURADA FCM POR PRIMERA VEZ:");
            addFCM( FirebaseInstanceId.getInstance().getToken() );
        }

        System.out.println("MAIN DIR FOTO " + rutaFotoPerfil);

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                arrayIdPublicacion   = new ArrayList<String>();
                arrayTitulo          = new ArrayList<String>();
                arrayNombreGrupo     = new ArrayList<String>();
                arrayComentario      = new ArrayList<String>();
                arrayLike            = new ArrayList<String>();
                arrayHoraPublicacion = new ArrayList<String>();
                arrayContenido       = new ArrayList<String>();
                arrayLink            = new ArrayList<String>();
                arrayWidth           = new ArrayList<String>();
                reloadConfesiones();
            }
        });
        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        cargarConfesiones();
        //detectarNuevasConfesiones();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);


        final Menu m = navigationView.getMenu();
        final SubMenu topChannelMenu = m.addSubMenu("Grupos Destacados");


        /**---------- Cambiar color menu----------*/
        MenuItem tools= m.findItem(R.id.gruposDestacados);
        SpannableString s = new SpannableString(tools.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.tituloCategoriaDrawer), 0, s.length(), 0);
        tools.setTitle(s);

        MenuItem tools2= m.findItem(R.id.acciones);
        SpannableString t = new SpannableString(tools2.getTitle());
        t.setSpan(new TextAppearanceSpan(this, R.style.tituloCategoriaDrawer), 0, t.length(), 0);
        tools2.setTitle(t);

        MenuItem tools3= m.findItem(R.id.desdePc);
        SpannableString u = new SpannableString(tools3.getTitle());
        u.setSpan(new TextAppearanceSpan(this, R.style.tituloCategoriaDrawer), 0, u.length(), 0);
        tools3.setTitle(u);
        /**---------------------------------------------------------*/


        if( rutaFotoPerfil.isEmpty() ){
            System.out.println("[]---------------------> ESVACIA");
        }else{
            System.out.println("[]---------------------> NOESVACIAA   " + "http://www.circlelabs.cl/app/confeciones/images/perfil/" + rutaFotoPerfil + ".jpeg" );
            NavigationView navigationView2 = (NavigationView) findViewById(R.id.nav_view);
            View headerView = navigationView2.getHeaderView(0);

            imagePerfilUsuario = (ImageView) headerView.findViewById(R.id.imagePerfilUsuario);
            tvNombreUsuario      = (TextView) headerView.findViewById(R.id.tvNombreUsuario);
            tvNombreUsuario.setText(nombreUsuario);
            Drawable man2 = getResources().getDrawable(R.drawable.man);

            Glide
                    .with( MainActivity.this )
                    .load( "http://www.circlelabs.cl/app/confeciones/images/perfil/" + rutaFotoPerfil + ".jpeg" )
                    .error(man2)
                    .thumbnail( 0.05f )
                    .fitCenter()
                    .bitmapTransform(new CropCircleTransformation(this))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into( imagePerfilUsuario );
        }


        if(sesionUsuario.isEmpty()){
            System.out.println("SESION VACIA");
            m.findItem(R.id.nav_editar_perfil).setVisible(false); //ESCONDER ITEM DEL MENU
            m.findItem(R.id.nav_mis_confesiones).setVisible(false);
            m.findItem(R.id.nav_cerrar_sesion).setVisible(false);
            m.findItem(R.id.nav_nuevo_grupo).setVisible(false);
            m.findItem(R.id.nav_login).setVisible(true);
            m.findItem(R.id.nav_registro).setVisible(true);
        }else{
            System.out.println("SESION NO VACIA");
            m.findItem(R.id.nav_editar_perfil).setVisible(true); //ESCONDER ITEM DEL MENU
            m.findItem(R.id.nav_mis_confesiones).setVisible(false);
            m.findItem(R.id.nav_cerrar_sesion).setVisible(true);
            m.findItem(R.id.nav_nuevo_grupo).setVisible(true);
            m.findItem(R.id.nav_login).setVisible(false);
            m.findItem(R.id.nav_registro).setVisible(false);
        }





        url = "http://www.circlelabs.cl/app/confeciones/getCategoria.php";
        HashMap<String, String> params = new HashMap<String, String>();
        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json3= new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                try {
                                    //progressDialog.dismiss();
                                    System.out.println("CATEGORIAS");
                                    System.out.println(response);
                                    JSONArray arrayResponse = response.getJSONArray("categoria");
                                    System.out.println(arrayResponse);



                                    MenuItem menu1 = m.findItem(R.id.nav_grupo_uno);
                                    menu1.setTitle(arrayResponse.getJSONObject(0).getString("nombre_categoria")).setIcon(R.drawable.primero);
                                    idCategoria1 = arrayResponse.getJSONObject(0).getString("id_categoria");

                                    MenuItem menu2 = m.findItem(R.id.nav_grupo_dos);
                                    menu2.setTitle(arrayResponse.getJSONObject(1).getString("nombre_categoria")).setIcon(R.drawable.segundo);
                                    idCategoria2 = arrayResponse.getJSONObject(1).getString("id_categoria");

                                    MenuItem menu3 = m.findItem(R.id.nav_grupo_tres);
                                    menu3.setTitle(arrayResponse.getJSONObject(2).getString("nombre_categoria")).setIcon(R.drawable.tercero);
                                    idCategoria3 = arrayResponse.getJSONObject(2).getString("id_categoria");


                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                    //progressDialog.dismiss();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                                //progressDialog.dismiss();

                            }
                        }
                );
        json3.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json3);

    }

    @Override
    protected void onResume() {
        super.onResume();
        // The activity has become visible (it is now "resumed").
        detectarNuevasConfesiones();
    }

    /*protected void onRestart() {
        super.onRestart();
        // The activity has become visible (it is now "resumed").
        System.out.println("--------------------------------------------------_>RESUME");
    }*/

    public void cargarConfesiones() {
        File file = new File( getCacheDir() + File.separator + "cacheFile.srl" );
        JSONObject jsonObj;
        if (file.exists()){
            try {
                ObjectInputStream in = new ObjectInputStream(new FileInputStream
                        (new File(getCacheDir() + File.separator + "cacheFile.srl")));
                jsonObj = new JSONObject((String) in.readObject());
                in.close();
                System.out.println("[CACHE]: " + jsonObj);

                JSONArray arrayResponse = jsonObj.getJSONArray("publicacion");

                for(int i=0; i<arrayResponse.length(); i++){
                    JSONObject objeto = arrayResponse.getJSONObject(i);

                    arrayNombreGrupo.add(objeto.getString("categoria"));
                    arrayContenido.add(objeto.getString("contenido"));
                    arrayComentario.add(objeto.getString("cantidad_comentarios"));
                    arrayLike.add(objeto.getString("likes"));
                    arrayHoraPublicacion.add(objeto.getString("fecha_publicacion"));
                    arrayTitulo.add(objeto.getString("titulo"));
                    arrayIdPublicacion.add(objeto.getString("id_publicacion"));
                    arrayLink.add(objeto.getString("link"));
                    arrayWidth.add(objeto.getString("width"));
                }

                //System.out.println(arrayNombreGrupo);

                //listViewConfesiones = (ListView) findViewById(R.id.contenedorListViewConfesiones);
                arrayAdapterConfesiones = new ListViewConfesiones(MainActivity.this,arrayIdPublicacion,arrayTitulo,arrayNombreGrupo,arrayContenido,arrayComentario,arrayLike,arrayHoraPublicacion,arrayLink,arrayWidth);
                listViewConfesiones.setAdapter(arrayAdapterConfesiones);

            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (OptionalDataException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (StreamCorruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else{
            System.out.println("NO EXCISTER");
            HashMap<String, String> params = new HashMap<String, String>();
            //params.put("id_publicacion", "1");
            String url = "http://www.circlelabs.cl/app/confeciones/getPublicacion.php";
            Drawable man = getResources().getDrawable(R.drawable.relojarena);

            progressDialog = new ProgressDialog(this);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setIndeterminateDrawable(man);
            progressDialog.setMessage("Cargando confesiones");
            progressDialog.show();

            final JSONObject parameters = new JSONObject(params);

            JsonObjectRequest json2 = new JsonObjectRequest
                    (url, parameters,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        /**---Almacenar en fuchero el response dle servidor para cargarlo mas rapido---**/
                                        try {
                                            ObjectOutput out = new ObjectOutputStream(new FileOutputStream
                                                    (new File(getCacheDir(), "") + File.separator + "cacheFile.srl"));
                                            out.writeObject(response.toString());
                                            out.close();
                                        } catch (FileNotFoundException e) {
                                            e.printStackTrace();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        /**---Almacenar en fuchero el response dle servidor para cargarlo mas rapido---**/

                                        JSONArray arrayResponse = response.getJSONArray("publicacion");

                                        for(int i=0; i<arrayResponse.length(); i++){
                                            JSONObject objeto = arrayResponse.getJSONObject(i);

                                            arrayNombreGrupo.add(objeto.getString("categoria"));
                                            arrayContenido.add(objeto.getString("contenido"));
                                            arrayComentario.add(objeto.getString("cantidad_comentarios"));
                                            arrayLike.add(objeto.getString("likes"));
                                            arrayHoraPublicacion.add(objeto.getString("fecha_publicacion"));
                                            arrayTitulo.add(objeto.getString("titulo"));
                                            arrayIdPublicacion.add(objeto.getString("id_publicacion"));
                                            arrayLink.add(objeto.getString("link"));
                                            arrayWidth.add(objeto.getString("width"));
                                        }

                                        //System.out.println(arrayNombreGrupo);

                                        //listViewConfesiones = (ListView) findViewById(R.id.contenedorListViewConfesiones);
                                        arrayAdapterConfesiones = new ListViewConfesiones(MainActivity.this,arrayIdPublicacion,arrayTitulo,arrayNombreGrupo,arrayContenido,arrayComentario,arrayLike,arrayHoraPublicacion,arrayLink,arrayWidth);
                                        listViewConfesiones.setAdapter(arrayAdapterConfesiones);
                                        progressDialog.dismiss();


                                    } catch (Exception e) {
                                        //loading.dismiss();
                                        Log.e("ERROR", "ERROR");
                                        e.printStackTrace();
                                        System.out.println(e.getMessage());
                                        progressDialog.dismiss();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // TODO Auto-generated method stub
                                    Log.e("VOLLEYeror", error.toString());
                                    requestQueue.stop();
                                    progressDialog.dismiss();
                                    String message = null;
                                    if (error instanceof NetworkError) {
                                        message = "Cannot connect to Internet...Please check your connection!";
                                        //Toast.makeText(MainActivity.this, message,
                                        //        Toast.LENGTH_SHORT).show();

                                        Drawable nointernet = getResources().getDrawable(R.drawable.nointernet);

                                        cancelDialog = new ProgressDialog(MainActivity.this);
                                        cancelDialog.setCanceledOnTouchOutside(false);
                                        cancelDialog.setIndeterminateDrawable(nointernet);
                                        cancelDialog.setMessage("No hay internet");
                                        cancelDialog.setButton("Entendido", new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(DialogInterface dialog, int which)
                                            {
                                                // Use either finish() or return() to either close the activity or just the dialog
                                                cancelDialog.dismiss();
                                                return;
                                            }
                                        });

                                        cancelDialog.show();
                                    } else if (error instanceof ServerError) {
                                        message = "The server could not be found. Please try again after some time!!";
                                        Toast.makeText(MainActivity.this, message,
                                                Toast.LENGTH_SHORT).show();
                                    } else if (error instanceof AuthFailureError) {
                                        message = "Cannot connect to Internet...Please check your connection!";
                                        Toast.makeText(MainActivity.this, message,
                                                Toast.LENGTH_SHORT).show();
                                    } else if (error instanceof ParseError) {
                                        message = "Parsing error! Please try again after some time!!";
                                        Toast.makeText(MainActivity.this, message,
                                                Toast.LENGTH_SHORT).show();
                                    } else if (error instanceof NoConnectionError) {
                                        message = "Cannot connect to Internet...Please check your connection!";
                                        Toast.makeText(MainActivity.this, message,
                                                Toast.LENGTH_SHORT).show();
                                    } else if (error instanceof TimeoutError) {
                                        message = "Connection TimeOut! Please check your internet connection.";
                                        Toast.makeText(MainActivity.this, message,
                                                Toast.LENGTH_SHORT).show();
                                    }
                                }
                            }
                    );
            json2.setRetryPolicy(new DefaultRetryPolicy(
                    5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            requestQueue.add(json2);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
            finish();
        } else {
            super.onBackPressed();
            finish();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, Nueva_publicacion.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_grupo_uno) {
            Intent intent = new Intent(this, Confesiones.class);
            String idCategoria = String.valueOf(idCategoria1);
            intent.putExtra(EXTRA_MESSAGE, idCategoria);
            intent.putExtra("NombreGrupoParam", item.getTitle());
            startActivity(intent);
        } else if (id == R.id.nav_grupo_dos) {
            Intent intent = new Intent(this, Confesiones.class);
            String idCategoria = String.valueOf(idCategoria2);
            intent.putExtra(EXTRA_MESSAGE, idCategoria);
            intent.putExtra("NombreGrupoParam", item.getTitle());
            startActivity(intent);
        } else if (id == R.id.nav_grupo_tres) {
            Intent intent = new Intent(this, Confesiones.class);
            String idCategoria = String.valueOf(idCategoria3);
            intent.putExtra(EXTRA_MESSAGE, idCategoria);
            intent.putExtra("NombreGrupoParam", item.getTitle());
            startActivity(intent);
        } else if(id == R.id.nav_ver_mas) {
            Intent intent = new Intent(this, VerMasCategoria.class);
            startActivity(intent);

        } else if (id == R.id.nav_mis_confesiones) {
            Intent intent = new Intent(this, Mis_confesiones.class);
            startActivity(intent);
        } else if (id == R.id.nav_nueva_confesion) {
            Intent intent = new Intent(this, Nueva_publicacion.class);
            startActivity(intent);
        } else if (id == R.id.nav_nuevo_grupo) {
            Intent intent = new Intent(this, Nuevo_grupo.class);
            startActivity(intent);
        } else if (id == R.id.nav_editar_perfil) {
            Intent intent = new Intent(this, Editar_perfil.class);
            startActivity(intent);
        }
        else if (id== R.id.nav_registro){
            Intent intent = new Intent(this, Registro.class);
            startActivity(intent);
        }
        else if (id== R.id.nav_login){
            Intent intent = new Intent(this, Login.class);
            startActivity(intent);
        }
        else if(id== R.id.nav_cerrar_sesion){
            manager.setPreferences(MainActivity.this, "sesionUsuario", "");
            manager.setPreferences(MainActivity.this, "sesionNombreUsuario","");
            manager.setPreferences(MainActivity.this, "sesionFotoUsuario","");
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_sitio_web) {
                /*
            final AlertDialog d = new AlertDialog.Builder(this)
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(R.drawable.ic_account_circle_black_24dp)
                    .setTitle("Publicar desde PC")
                    .setMessage(Html.fromHtml("<a href=\"http://www.google.com\">www.google.com</a>"))
                    .create();
            d.show();
            */
            String url = "http://www.circlelabs.cl/confesiones";
            try {
                Uri uri = Uri.parse("googlechrome://navigate?url=" + url);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            } catch (ActivityNotFoundException e) {
                // Chrome is probably not installed
            }
            // Make the textview clickable. Must be called after show()
            //((TextView)d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());
        } else if (id == R.id.nav_facebook) {
            /*final AlertDialog d = new AlertDialog.Builder(this)
                    .setPositiveButton(android.R.string.ok, null)
                    .setIcon(R.drawable.ic_account_circle_black_24dp)
                    .setTitle("Publicar desde PC")
                    .setMessage(Html.fromHtml("<a href=\"http://www.google.com\">www.google.com</a>"))
                    .create();
            d.show();*/
            // Make the textview clickable. Must be called after show()
            //((TextView)d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());

            String url = "https://www.facebook.com/Confesiones-APP-1063827627083423/";
            try {
                Uri uri = Uri.parse("googlechrome://navigate?url=" + url);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            } catch (ActivityNotFoundException e) {
                // Chrome is probably not installed
            }
        }








        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void reloadConfesiones(){
        String url = "http://www.circlelabs.cl/app/confeciones/getPublicacion.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        HashMap<String, String> params = new HashMap<String, String>();
        //params.put("id_publicacion", "1");

        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                System.out.println(response);
                                try {

                                    /**---Almacenar en fuchero el response dle servidor para cargarlo mas rapido---**/
                                    try {
                                        ObjectOutput out = new ObjectOutputStream(new FileOutputStream
                                                (new File(getCacheDir(), "") + File.separator + "cacheFile.srl"));
                                        out.writeObject(response.toString());
                                        out.close();
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    /**---Almacenar en fuchero el response dle servidor para cargarlo mas rapido---**/

                                    System.out.println(response);
                                    JSONArray arrayResponse = response.getJSONArray("publicacion");
                                    //System.out.println(arrayResponse);



                                    for(int i=0; i<arrayResponse.length(); i++){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);

                                        arrayNombreGrupo.add(objeto.getString("categoria"));
                                        arrayContenido.add(objeto.getString("contenido"));
                                        arrayComentario.add(objeto.getString("cantidad_comentarios"));
                                        arrayLike.add(objeto.getString("likes"));
                                        arrayHoraPublicacion.add(objeto.getString("fecha_publicacion"));
                                        arrayTitulo.add(objeto.getString("titulo"));
                                        arrayIdPublicacion.add(objeto.getString("id_publicacion"));
                                        arrayLink.add(objeto.getString("link"));
                                        arrayWidth.add(objeto.getString("width"));
                                    }

                                    System.out.println(arrayNombreGrupo);

                                    //listViewConfesiones = (ListView) findViewById(R.id.contenedorListViewConfesiones);
                                    arrayAdapterConfesiones = new ListViewConfesiones(MainActivity.this,arrayIdPublicacion,arrayTitulo,arrayNombreGrupo,arrayContenido,arrayComentario,arrayLike,arrayHoraPublicacion,arrayLink,arrayWidth);
                                    listViewConfesiones.setAdapter(arrayAdapterConfesiones);

                                    swipeContainer.setRefreshing(false);

                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println("ERROR GETPUBLICACION.PHP");
                                    System.out.println(e.getMessage());
                                    swipeContainer.setRefreshing(false);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                System.out.println("ERROR GETPUBLICACION.PHP");
                                requestQueue.stop();
                                swipeContainer.setRefreshing(false);
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);
    }

    public void reloadNuevasConfesiones(){

        Drawable man = getResources().getDrawable(R.drawable.relojarena);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminateDrawable(man);
        progressDialog.setMessage("Cargando confesiones");
        progressDialog.show();

        String url = "http://www.circlelabs.cl/app/confeciones/getPublicacion.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        HashMap<String, String> params = new HashMap<String, String>();
        //params.put("id_publicacion", "1");

        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                try {

                                    /**---Almacenar en fuchero el response dle servidor para cargarlo mas rapido---**/
                                    try {
                                        ObjectOutput out = new ObjectOutputStream(new FileOutputStream
                                                (new File(getCacheDir(), "") + File.separator + "cacheFile.srl"));
                                        out.writeObject(response.toString());
                                        out.close();
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    /**---Almacenar en fuchero el response dle servidor para cargarlo mas rapido---**/

                                    //System.out.println(response);
                                    JSONArray arrayResponse = response.getJSONArray("publicacion");
                                    //System.out.println(arrayResponse);



                                    for(int i=0; i<arrayResponse.length(); i++){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);

                                        arrayNombreGrupo.add(objeto.getString("categoria"));
                                        arrayContenido.add(objeto.getString("contenido"));
                                        arrayComentario.add(objeto.getString("cantidad_comentarios"));
                                        arrayLike.add(objeto.getString("likes"));
                                        arrayHoraPublicacion.add(objeto.getString("fecha_publicacion"));
                                        arrayTitulo.add(objeto.getString("titulo"));
                                        arrayIdPublicacion.add(objeto.getString("id_publicacion"));
                                        arrayLink.add(objeto.getString("link"));
                                        arrayWidth.add(objeto.getString("width"));
                                    }

                                    System.out.println(arrayNombreGrupo);

                                    arrayAdapterConfesiones = new ListViewConfesiones(MainActivity.this,arrayIdPublicacion,arrayTitulo,arrayNombreGrupo,arrayContenido,arrayComentario,arrayLike,arrayHoraPublicacion,arrayLink,arrayWidth);
                                    listViewConfesiones.setAdapter(arrayAdapterConfesiones);

                                    progressDialog.dismiss();
                                    relativeNuevaConfesion.setVisibility(View.GONE);
                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                    progressDialog.dismiss();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                                progressDialog.dismiss();
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);
    }

    public void detectarNuevasConfesiones(){
        String url = "http://www.circlelabs.cl/app/confeciones/detectarNuevasConfesiones.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        HashMap<String, String> params = new HashMap<String, String>();
        //params.put("id_publicacion", "1");

        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                String id_publicacion = "";
                                try {

                                    System.out.println("detectar nuevas confesiones: " + response);
                                    JSONArray arrayResponse = response.getJSONArray("publicacion");
                                    //System.out.println(arrayResponse);


                                    for(int i=0; i<arrayResponse.length(); i++){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);
                                        id_publicacion = objeto.getString("id_publicacion");
                                    }

                                    //conseguir el ultimo id guardado
                                    //arrayIdPublicacion.get( 0 );
                                    //System.out.println("Ñ]Ñ]Ñ]Ñ]Ñ]Ñ] --------------__Z>   " + id_publicacion);
                                    //Validar si hay arreglo de confesiones...
                                    if( arrayIdPublicacion.size() != 0 ){
                                        if( arrayIdPublicacion.get(0).equals(id_publicacion) ){
                                            //no motrar el mensaje de recargar
                                        }else{
                                            //mostrar el mensaje de recargar
                                            relativeNuevaConfesion.setVisibility(View.VISIBLE);
                                        }
                                    }


                                    //listViewConfesiones = (ListView) findViewById(R.id.contenedorListViewConfesiones);
                                    arrayAdapterConfesiones = new ListViewConfesiones(MainActivity.this,arrayIdPublicacion,arrayTitulo,arrayNombreGrupo,arrayContenido,arrayComentario,arrayLike,arrayHoraPublicacion,arrayLink,arrayWidth);
                                    listViewConfesiones.setAdapter(arrayAdapterConfesiones);

                                    swipeContainer.setRefreshing(false);

                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                    swipeContainer.setRefreshing(false);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                                swipeContainer.setRefreshing(false);
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);
    }

    public void getConfesionesPaginadas(final String pagina){

        Drawable man = getResources().getDrawable(R.drawable.relojarena);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminateDrawable(man);
        progressDialog.setMessage("Cargando confesiones");
        progressDialog.show();

        String url = "http://www.circlelabs.cl/app/confeciones/getPublicacionPaginada.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("pagina", pagina);
        params.put("id_usuario_activo", sesionUsuario);

        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                try {
                                    System.out.println(response);
                                    /**---Almacenar en fuchero el response dle servidor para cargarlo mas rapido---**/
                                    try {
                                        ObjectOutput out = new ObjectOutputStream(new FileOutputStream
                                                (new File(getCacheDir(), "") + File.separator + "cacheFile.srl"));
                                        out.writeObject(response.toString());
                                        out.close();
                                    } catch (FileNotFoundException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    /**---Almacenar en fuchero el response dle servidor para cargarlo mas rapido---**/

                                    //System.out.println(response);
                                    JSONArray arrayResponse = response.getJSONArray("publicacion");
                                    //System.out.println(arrayResponse);



                                    for(int i=0; i<arrayResponse.length(); i++){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);

                                        arrayNombreGrupo.add(objeto.getString("categoria"));
                                        arrayContenido.add(objeto.getString("contenido"));
                                        arrayComentario.add(objeto.getString("cantidad_comentarios"));
                                        arrayLike.add(objeto.getString("likes"));
                                        arrayHoraPublicacion.add(objeto.getString("fecha_publicacion"));
                                        arrayTitulo.add(objeto.getString("titulo"));
                                        arrayIdPublicacion.add(objeto.getString("id_publicacion"));
                                        arrayLink.add(objeto.getString("link"));
                                        arrayWidth.add(objeto.getString("width"));
                                    }


                                    listViewConfesiones = (ListView) findViewById(R.id.contenedorListViewConfesiones);
                                    arrayAdapterConfesiones = new ListViewConfesiones(MainActivity.this,arrayIdPublicacion,arrayTitulo,arrayNombreGrupo,arrayContenido,arrayComentario,arrayLike,arrayHoraPublicacion,arrayLink,arrayWidth);
                                    listViewConfesiones.setAdapter(arrayAdapterConfesiones);
                                    listViewConfesiones.smoothScrollToPosition(Integer.parseInt(pagina) -21 );

                                    progressDialog.dismiss();

                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                    progressDialog.dismiss();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);
    }

    public void addFCM(final String token){

        String url = "http://www.circlelabs.cl/app/confeciones/addFCM.php";

        requestQueue = Volley.newRequestQueue(this);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("fcm", token);
        params.put("id_usuario_activo", sesionUsuario);
        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    //System.out.println("-----------------------------------------_>: " + sesionUsuario + " " +token);
                                    //System.out.println("respuesta-----------------------------------------_>: " + response);
                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);


    }
}
