package com.circlelabs.app.confesiones;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class Editar_perfil extends AppCompatActivity {

    Button btnEditarPerfil;
    EditText etApodo,etEmail;
    TextView tvApodo;
    RequestQueue requestQueue;
    ImageView fotoPerfil;
    Uri selectedImageUri;
    Bitmap bitmap;
    String encodedString,rutaFotoPerfil,idUsuario;
    RequestQueue rq;
    SessionManager manager;
    ProgressDialog progressDialog;
    int cambioImagen = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);

        Drawable man = getResources().getDrawable(R.drawable.relojarena);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminateDrawable(man);
        progressDialog.setMessage("Cargando perfil");
        progressDialog.show();

        fotoPerfil = (ImageView) findViewById(R.id.imagen_perfil);
        fotoPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(shouldAskPermission() == true) {

                    String[] perms = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
                    int permsRequestCode = 200;
                    ActivityCompat.requestPermissions(Editar_perfil.this,perms, permsRequestCode);

                }

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent,"Elije tu foto de perfil "), 10);

            }
        });

        //cambiar nombre del action bar
        getSupportActionBar().setTitle("Editar perfil");
        getSupportActionBar().setElevation(0);
        //Habilitar la flechita para atras
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable( new ColorDrawable( Color.parseColor("#322f60") ) );

        manager = new SessionManager();
        rutaFotoPerfil = manager.getPreferences(Editar_perfil.this,"sesionFotoUsuario");
        idUsuario = manager.getPreferences(Editar_perfil.this,"sesionUsuario");


        etApodo = (EditText) findViewById(R.id.etApodo);
        etEmail = (EditText) findViewById(R.id.etEmail);
        tvApodo = (TextView) findViewById(R.id.tvApodo);
        btnEditarPerfil = (Button) findViewById(R.id.btnEditarPerfil);

        cargarPerfilUsuario();

        btnEditarPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*String apodo,edad,email;
                apodo = etApodo.getText().toString();
                email = etEmail.getText().toString();
                Toast.makeText(Editar_perfil.this, apodo + " " + email,
                        Toast.LENGTH_SHORT).show();*/
                editarPerfil();
            }
        });
    }

    public void cargarPerfilUsuario(){

        progressDialog.show();
        Drawable man = getResources().getDrawable(R.drawable.man);

        Glide
        .with( Editar_perfil.this )
                .load( "http://www.circlelabs.cl/app/confeciones/images/perfil/" + rutaFotoPerfil + ".jpeg" )
                .error(man)
                .thumbnail( 0.05f )
                .fitCenter()
                .bitmapTransform(new CropCircleTransformation(this))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into( fotoPerfil );

        String url = "http://www.circlelabs.cl/app/confeciones/getPerfil.php";
        requestQueue = Volley.newRequestQueue(this);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id_usuario_activo", idUsuario);
        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    JSONArray arrayResponse = response.getJSONArray("usuario");
                                    //System.out.println(arrayResponse);

                                    for(int i=0; i<arrayResponse.length(); i++){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);
                                        tvApodo.setText(objeto.getString("nombre"));
                                         etApodo.setHint(objeto.getString("nombre"));
                                    }
                                    progressDialog.dismiss();

                                } catch (Exception e) {
                                    //loading.dismiss();
                                    progressDialog.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                                progressDialog.dismiss();
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);
    }

    public void editarPerfil(){
        final String apodo, email;

        if( etApodo.getText().toString().equals("") ){
            apodo = etApodo.getHint().toString();
        }else{
            apodo = etApodo.getText().toString();
        }

        if( etEmail.getText().toString().equals("") ){
            email= etEmail.getHint().toString();
        }else{
            email = etEmail.getText().toString();
        }

        if( etEmail.getText().toString().equals("") && etApodo.getText().toString().equals("") && cambioImagen == 0){
            Toast.makeText(Editar_perfil.this, "Debes hacer algún cambio",
                    Toast.LENGTH_SHORT).show();
        }else{
            Drawable tiempo = getResources().getDrawable(R.drawable.relojarena);
            progressDialog = new ProgressDialog(this);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setIndeterminateDrawable(tiempo);
            progressDialog.setMessage("Actualizando perfil");
            progressDialog.show();

            String url = "http://www.circlelabs.cl/app/confeciones/modificarPerfil.php";
            requestQueue = Volley.newRequestQueue(this);
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("id_usuario_activo", idUsuario);
            params.put("apodo", apodo);
            params.put("clave", email);
            final JSONObject parameters = new JSONObject(params);

            JsonObjectRequest json2 = new JsonObjectRequest
                    (url, parameters,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        final ProgressDialog cancelDialog;
                                        progressDialog.dismiss();

                                        //System.out.println("[---------------------] " + response );
                                        JSONObject arrayResponse = response.getJSONObject("usuario");
                                        //System.out.println( arrayResponse.getString("resultado") );

                                        if(arrayResponse.getString("resultado").equals("ok") ){
                                            etApodo.setText("");
                                            etApodo.setHint( apodo );

                                            etEmail.setText("");
                                            etEmail.setHint( "Cambiar contraseña" );

                                            tvApodo.setText(apodo);

                                            manager.setPreferences(Editar_perfil.this, "sesionNombreUsuario",apodo);

                                            Drawable nointernet = getResources().getDrawable(R.drawable.clover);
                                            cancelDialog = new ProgressDialog(Editar_perfil.this);
                                            cancelDialog.setCanceledOnTouchOutside(false);
                                            cancelDialog.setIndeterminateDrawable(nointernet);
                                            cancelDialog.setMessage("Perfil actualizado");
                                            cancelDialog.setButton("Entendido", new DialogInterface.OnClickListener()
                                            {
                                                public void onClick(DialogInterface dialog, int which)
                                                {
                                                    // Use either finish() or return() to either close the activity or just the dialog
                                                    cancelDialog.dismiss();
                                                    return;
                                                }
                                            });
                                            cancelDialog.show();

                                        }else{
                                            Drawable nointernet = getResources().getDrawable(R.drawable.delete);
                                            cancelDialog = new ProgressDialog(Editar_perfil.this);
                                            cancelDialog.setCanceledOnTouchOutside(false);
                                            cancelDialog.setIndeterminateDrawable(nointernet);
                                            cancelDialog.setMessage("Ha ocurrido un error, intenta nuevamente");
                                            cancelDialog.setButton("Entendido", new DialogInterface.OnClickListener()
                                            {
                                                public void onClick(DialogInterface dialog, int which)
                                                {
                                                    // Use either finish() or return() to either close the activity or just the dialog
                                                    cancelDialog.dismiss();
                                                    return;
                                                }
                                            });
                                            cancelDialog.show();
                                        }

                                    } catch (Exception e) {
                                        //loading.dismiss();
                                        Log.e("ERROR", "ERROR");
                                        e.printStackTrace();
                                        System.out.println(e.getMessage());
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // TODO Auto-generated method stub
                                    Log.e("VOLLEYeror", error.toString());
                                    requestQueue.stop();
                                }
                            }
                    );
            json2.setRetryPolicy(new DefaultRetryPolicy(
                    5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            requestQueue.add(json2);
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == -1) {


            if (data.getData() != null) {
                selectedImageUri = data.getData();


                Uri pickedImage = data.getData();
                // Let's read picked image path using content resolver
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor cursor = Editar_perfil.this.getContentResolver().query(pickedImage, filePath, null, null, null);
                cursor.moveToFirst();
                String imagePath = cursor.getString(cursor.getColumnIndex(filePath[0]));

                ImageView imageView = (ImageView) findViewById(R.id.imagen_perfil);
                Glide
                        .with( Editar_perfil.this )
                        .load( new File( imagePath ))
                        .thumbnail( 0.05f )
                        .fitCenter()
                        .bitmapTransform(new CropCircleTransformation(this))
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into( imageView );

                //imageView.setImageBitmap(BitmapFactory.decodeFile(imagePath));
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(Editar_perfil.this.getContentResolver(), data.getData());
                } catch (IOException e) {
                    e.printStackTrace();
                }


                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                Bitmap bitmap = BitmapFactory.decodeFile(imagePath, options);

                //LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(100, 100);
                //imageView.setLayoutParams(layoutParams);
                //imageView.setImageBitmap(bitmap);


                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                byte[] bytes = stream.toByteArray();
                encodedString = Base64.encodeToString(bytes, 0);

                uploadImage(encodedString);

                cursor.close();
            } else {
                Log.d("selectedPath1 : ", "Came here its null !");
                Toast.makeText(Editar_perfil.this.getApplicationContext(), "failed to get Image!", Toast.LENGTH_LONG).show();
            }

        }


    }


    public void uploadImage(String encodedString) {

        //final ProgressDialog loading = ProgressDialog.show(Editar_perfil.this, "Subiendo imagen", "Espere por favor", false, false);

        Drawable man = getResources().getDrawable(R.drawable.relojarena);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminateDrawable(man);
        progressDialog.setMessage("Agregando imagen");
        progressDialog.show();

        rq = Volley.newRequestQueue(Editar_perfil.this);

        HashMap<String, String> params2 = new HashMap<String, String>();
        params2.put("foto", encodedString);
        params2.put("id_usuario", idUsuario);

        final JSONObject parameters = new JSONObject(params2);

        JsonObjectRequest json2 = new JsonObjectRequest
                ("http://www.circlelabs.cl/app/confeciones/uploadImagePerfil.php", parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response7) {

                                try {

                                    progressDialog.dismiss();
                                    System.out.println(response7);
                                    //System.out.println("RESPONSE ENVIAR4:"+ response7);
                                    JSONObject imagenes = response7.getJSONObject("publicacion");

                                    String res = imagenes.getString("resultado");
                                    String fotoUsuario = imagenes.getString("fotoUsuario");
                                    if(res.equals("ok")) {
                                        cambioImagen = 1;
                                        manager.setPreferences(Editar_perfil.this, "sesionFotoUsuario", fotoUsuario);
                                    }

                                    //String fotoUsuario = resultado.getString("fotoUsuario");


                                    //llenarSpinner(regiones);
                                    //progressDialog.hide();
                                } catch (Exception e) {
                                    progressDialog.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                rq.stop();
                            }
                        }
                );

        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        rq.add(json2);
    }

    private boolean shouldAskPermission(){

        return(Build.VERSION.SDK_INT> Build.VERSION_CODES.LOLLIPOP_MR1);

    }
}
