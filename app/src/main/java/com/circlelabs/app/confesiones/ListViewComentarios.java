package com.circlelabs.app.confesiones;


/**
 * Created by Fabian on 24/06/2016.
 */
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;


import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.danikula.videocache.HttpProxyCacheServer;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;

import jp.wasabeef.glide.transformations.CropCircleTransformation;

public class ListViewComentarios extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> idComentario;
    private final ArrayList<String> idUsuario;
    private final ArrayList<String> nombreUsuario;
    private final ArrayList<String> comentario;
    private final ArrayList<String> fecha;
    private final ArrayList<String> foto;
    private final ArrayList<String> link;
    private final ArrayList<String> width;

    public ListViewComentarios(
            Activity context,
            ArrayList<String> idComentario,
            ArrayList<String> idUsuario,
            ArrayList<String> nombreUsuario,
            ArrayList<String> comentario,
            ArrayList<String> fecha,
            ArrayList<String> foto,
            ArrayList<String> link,
            ArrayList<String> width) {
        super(context, R.layout.list_view_confesiones,idComentario);
        this.context = context;
        this.idComentario = idComentario;
        this.idUsuario = idUsuario;
        this.nombreUsuario = nombreUsuario;
        this.comentario = comentario;
        this.fecha = fecha;
        this.foto = foto;
        this.link = link;
        this.width = width;
    }

    TextView tvUsuario,tvComentario,tvFecha;
    ImageView fotoUsuario;
    LinearLayout contenedorGifSeleccionado;
    VideoView gifComentario;

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();

        final View rowView= inflater.inflate(R.layout.activity_list_view_comentarios, null, true);

        tvUsuario    = (TextView) rowView.findViewById(R.id.tvUsuario);
        tvComentario = (TextView) rowView.findViewById(R.id.tvComentario);
        tvFecha      = (TextView) rowView.findViewById(R.id.tvFecha);
        fotoUsuario  = (ImageView) rowView.findViewById(R.id.fotoUsuario);
        contenedorGifSeleccionado = (LinearLayout) rowView.findViewById(R.id.contenedorGifSeleccionado);
        gifComentario = (VideoView) rowView.findViewById(R.id.gifComentario);


        //asignar gif si existe el link
        if( !link.get(position).equals("")  ){
            System.out.println( "Deberia entrar una sola vezs " );
            contenedorGifSeleccionado.setVisibility(View.VISIBLE);

            //Uri video = Uri.parse(link.get(position));
            //gifComentario.setVideoURI(video);

            HttpProxyCacheServer proxy = ProxyFactory.getProxy(context);
            String proxyUrl = proxy.getProxyUrl(link.get(position));
            gifComentario.setVideoPath(proxyUrl);

            if( !width.get(position).equals("") )
                gifComentario.getLayoutParams().height = Integer.parseInt(  width.get(position).toString() );


            gifComentario.start();
        }
        gifComentario.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });



        //asignar valores a los textos
        tvComentario.setText(comentario.get(position));
        tvUsuario.setText(nombreUsuario.get(position));
        tvFecha.setText(fecha.get(position));

        Drawable man = context.getResources().getDrawable(R.drawable.man);
        Glide
                .with( context )
                .load( "http://www.circlelabs.cl/app/confeciones/images/perfil/" + foto.get(position) + ".jpeg" )
                .error(man)
                .thumbnail( 0.05f )
                .fitCenter()
                .bitmapTransform(new CropCircleTransformation(context))
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into( fotoUsuario );

        return rowView;
    }

}