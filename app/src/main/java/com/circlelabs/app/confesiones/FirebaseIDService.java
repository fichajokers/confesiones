package com.circlelabs.app.confesiones;

import android.util.Log;
import android.widget.ArrayAdapter;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by matias on 20-06-17.
 */
public class FirebaseIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";
    RequestQueue requestQueue;
    String sesionUsuario;
    SessionManager manager;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
        System.out.println("TOKEN FCM: " + token);
        addFCM(token);
    }

    public void addFCM(String token){

        manager = new SessionManager();
        sesionUsuario = manager.getPreferences(this, "sesionUsuario");
        manager.setPreferences(this,"FCM",token);


            String url = "http://www.circlelabs.cl/app/confeciones/addFCM.php";

            requestQueue = Volley.newRequestQueue(this);
            HashMap<String, String> params = new HashMap<String, String>();
            final JSONObject parameters = new JSONObject(params);
            params.put("fcm", token);
            params.put("id_usuario_activo", sesionUsuario);

            JsonObjectRequest json2 = new JsonObjectRequest
                    (url, parameters,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {

                                        System.out.print("respuesta:");
                                        System.out.print(response);

                                    } catch (Exception e) {
                                        //loading.dismiss();
                                        Log.e("ERROR", "ERROR");
                                        e.printStackTrace();
                                        System.out.println(e.getMessage());
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // TODO Auto-generated method stub
                                    Log.e("VOLLEYeror", error.toString());
                                    requestQueue.stop();
                                }
                            }
                    );
            json2.setRetryPolicy(new DefaultRetryPolicy(
                    5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            requestQueue.add(json2);


    }
}