package com.circlelabs.app.confesiones;

/**
 * Created by matias on 16-08-17.
 */

import android.content.Context;

import com.danikula.videocache.HttpProxyCacheServer;

/**
 * <strong>Not thread-safe</strong> {@link HttpProxyCacheServer} factory that returns single instance of proxy.
 *
 * @author Alexey Danilov (danikula@gmail.com).
 */
public class ProxyFactory {

    private static HttpProxyCacheServer sharedProxy;

    private ProxyFactory() {
    }

    public static HttpProxyCacheServer getProxy(Context context) {
        return sharedProxy == null ? (sharedProxy = newProxy(context)) : sharedProxy;
    }

    private static HttpProxyCacheServer newProxy(Context context) {
        return new HttpProxyCacheServer(context);
    }
}