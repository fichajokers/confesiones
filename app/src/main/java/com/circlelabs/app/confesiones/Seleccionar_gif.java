package com.circlelabs.app.confesiones;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;

public class Seleccionar_gif extends AppCompatActivity {

    VideoView videoView1,videoView2,videoView3,videoView4,videoView5,videoView6,videoView7;
    VideoView gifSeleccionadoPopup;
    String link1,link2,link3,link4,link5,link6,link7;
    String width1,width2,width3,width4,width5,width6,width7;
    EditText etBuscarGif;
    HorizontalScrollView scrollViewGif;
    LinearLayout contenedorGifSeleccionadoPopup;
    String[] linkGifSeleccionado = {null};
    String[] widthGifSeleccionado = {null};
    Button btnSubirGif;
    String idPublicacion,idUsuario;
    RequestQueue requestQueue;

    long startClickTime;
    final int MAX_CLICK_DURATION = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_gif);

        Intent myIntent = getIntent();
        idPublicacion   = myIntent.getStringExtra("idPublicacion");
        idUsuario       = myIntent.getStringExtra("idUsuario");


        btnSubirGif = (Button) findViewById(R.id.btnSubirGif);
        contenedorGifSeleccionadoPopup = (LinearLayout) findViewById(R.id.contenedorGifSeleccionado);
        gifSeleccionadoPopup = (VideoView) findViewById(R.id.gifSeleccionadoPopup);
        scrollViewGif = (HorizontalScrollView) findViewById(R.id.horizontalScrollViewGif);
        etBuscarGif = (EditText) findViewById(R.id.etBuscarGif);
        videoView1 = (VideoView) findViewById(R.id.vidGiphy1);
        videoView2 = (VideoView) findViewById(R.id.vidGiphy2);
        videoView3 = (VideoView) findViewById(R.id.vidGiphy3);
        videoView4 = (VideoView) findViewById(R.id.vidGiphy4);
        videoView5 = (VideoView) findViewById(R.id.vidGiphy5);
        videoView6 = (VideoView) findViewById(R.id.vidGiphy6);
        videoView7 = (VideoView) findViewById(R.id.vidGiphy7);

        btnSubirGif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println( linkGifSeleccionado[0] + " " + widthGifSeleccionado[0] );
                addComentario(linkGifSeleccionado[0],widthGifSeleccionado[0]);
            }
        });

        gifSeleccionadoPopup.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

        scrollViewGif.postDelayed(new Runnable() {
            public void run() {
                scrollViewGif.fullScroll(HorizontalScrollView.FOCUS_LEFT);
            }
        }, 100L);

        etBuscarGif.setInputType(InputType.TYPE_CLASS_TEXT);
        etBuscarGif.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // If the event is a key-down event on the "enter" button
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    System.out.println( etBuscarGif.getText().toString() );
                    //esconder el contenedor del gif seleccionado
                    //contenedorGifSeleccionadoPopup.setVisibility(View.GONE);

                    //Llamar a buscar el gif
                    scrollViewGif.setVisibility(View.VISIBLE);
                    getGiphy(etBuscarGif.getText().toString());
                    scrollViewGif.fullScroll(HorizontalScrollView.FOCUS_LEFT);

                    // Check if no view has focus:
                    View view = v;
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    return true;
                }
                return false;
            }
        });

        videoView1.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView1.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();

                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            System.out.println(link1);

                            linkGifSeleccionado[0] = link1;
                            widthGifSeleccionado[0] = width1;
                            Uri video = Uri.parse(link1);

                            gifSeleccionadoPopup.getLayoutParams().height = Integer.parseInt(width1);
                            gifSeleccionadoPopup.setVideoURI(video);
                            gifSeleccionadoPopup.start();

                            contenedorGifSeleccionadoPopup.setVisibility(View.VISIBLE);
                            scrollViewGif.setVisibility(View.GONE);
                            btnSubirGif.setVisibility(View.VISIBLE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;
            }
        });

        videoView2.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();

                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            System.out.println(link2);

                            linkGifSeleccionado[0] = link2;
                            widthGifSeleccionado[0] = width2;
                            Uri video = Uri.parse(link2);
                            gifSeleccionadoPopup.getLayoutParams().height = Integer.parseInt(width2);
                            gifSeleccionadoPopup.setVideoURI(video);
                            gifSeleccionadoPopup.start();
                            contenedorGifSeleccionadoPopup.setVisibility(View.VISIBLE);
                            scrollViewGif.setVisibility(View.GONE);
                            btnSubirGif.setVisibility(View.VISIBLE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;
            }
        });

        videoView3.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView3.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                System.out.println(link3);

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();

                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            linkGifSeleccionado[0] = link3;
                            widthGifSeleccionado[0] = width3;
                            Uri video = Uri.parse(link3);
                            gifSeleccionadoPopup.getLayoutParams().height = Integer.parseInt(width3);
                            gifSeleccionadoPopup.setVideoURI(video);
                            gifSeleccionadoPopup.start();
                            contenedorGifSeleccionadoPopup.setVisibility(View.VISIBLE);
                            scrollViewGif.setVisibility(View.GONE);
                            btnSubirGif.setVisibility(View.VISIBLE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;
            }
        });

        videoView4.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView4.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();

                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            System.out.println(link4);

                            linkGifSeleccionado[0] = link4;
                            widthGifSeleccionado[0] = width4;
                            Uri video = Uri.parse(link4);
                            gifSeleccionadoPopup.getLayoutParams().height = Integer.parseInt(width4);
                            gifSeleccionadoPopup.setVideoURI(video);
                            gifSeleccionadoPopup.start();
                            contenedorGifSeleccionadoPopup.setVisibility(View.VISIBLE);
                            scrollViewGif.setVisibility(View.GONE);
                            btnSubirGif.setVisibility(View.VISIBLE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;
            }
        });

        videoView5.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView5.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();

                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            System.out.println(link5);

                            linkGifSeleccionado[0] = link5;
                            widthGifSeleccionado[0] = width5;
                            Uri video = Uri.parse(link5);
                            gifSeleccionadoPopup.getLayoutParams().height = Integer.parseInt(width5);
                            gifSeleccionadoPopup.setVideoURI(video);
                            gifSeleccionadoPopup.start();
                            contenedorGifSeleccionadoPopup.setVisibility(View.VISIBLE);
                            scrollViewGif.setVisibility(View.GONE);
                            btnSubirGif.setVisibility(View.VISIBLE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;
            }
        });

        videoView6.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView6.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();

                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            System.out.println(link6);

                            linkGifSeleccionado[0] = link6;
                            widthGifSeleccionado[0] = width6;
                            Uri video = Uri.parse(link6);
                            gifSeleccionadoPopup.getLayoutParams().height = Integer.parseInt(width6);
                            gifSeleccionadoPopup.setVideoURI(video);
                            gifSeleccionadoPopup.start();
                            contenedorGifSeleccionadoPopup.setVisibility(View.VISIBLE);
                            scrollViewGif.setVisibility(View.GONE);
                            btnSubirGif.setVisibility(View.VISIBLE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;
            }
        });

        videoView7.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });
        videoView7.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // Write your code to perform an action on down
                        startClickTime = Calendar.getInstance().getTimeInMillis();

                        break;
                    case MotionEvent.ACTION_MOVE:
                        // Write your code to perform an action on contineus touch move
                        break;
                    case MotionEvent.ACTION_UP:
                        // Write your code to perform an action on touch up
                        long clickDuration = Calendar.getInstance().getTimeInMillis() - startClickTime;
                        if(clickDuration < MAX_CLICK_DURATION) {
                            System.out.println(link7);

                            linkGifSeleccionado[0] = link7;
                            widthGifSeleccionado[0] = width7;
                            Uri video = Uri.parse(link7);
                            gifSeleccionadoPopup.getLayoutParams().height = Integer.parseInt(width7);
                            gifSeleccionadoPopup.setVideoURI(video);
                            gifSeleccionadoPopup.start();
                            contenedorGifSeleccionadoPopup.setVisibility(View.VISIBLE);
                            scrollViewGif.setVisibility(View.GONE);
                            btnSubirGif.setVisibility(View.VISIBLE);
                            etBuscarGif.setText("");
                        }
                        break;
                }

                return true;
            }
        });
    }

    public void getGiphy(String busqeda) {
        final RequestQueue rq = Volley.newRequestQueue( getApplicationContext() );
        String link = "https://api.giphy.com/v1/gifs/search?api_key=befb8defdf2b4728a9d211429bab5a81&q="+busqeda+"&limit=10&offset=0&rating=G&lang=es";
        HashMap<String, String> params2 = new HashMap<String, String>();

        final JSONObject parameters = new JSONObject(params2);
        JsonObjectRequest json2 = new JsonObjectRequest
                (link, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response7) {
                                try {
                                    //System.out.println(response7);
                                    JSONArray arrayResponse = response7.getJSONArray("data");

                                    for(int i=0; i<arrayResponse.length(); i++){
                                        //if(i==0){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);

                                        JSONObject objeto2 = objeto.getJSONObject("images");
                                        //System.out.println( objeto2.getString("downsized_small") );
                                        //System.out.println( objeto2.getString("original") );

                                        //JSONObject objeto3 = objeto2.getJSONObject("downsized_small");
                                        JSONObject objeto3 = objeto2.getJSONObject("fixed_width");
                                        System.out.println( objeto3.getString("mp4") );

                                            /*JSONObject objeto4 = objeto2.getJSONObject("original");
                                            System.out.println( objeto4.getString("mp4") );*/

                                        System.out.println("------------------------");

                                        String LINK = objeto3.getString("mp4").toString();
                                        String WIDTH = objeto3.getString("width").toString();

                                            /*MediaController mc = new MediaController(getApplicationContext());
                                            mc.setAnchorView(videoView);
                                            mc.setMediaPlayer(videoView);*/
                                        Uri video = Uri.parse(LINK);
                                        //videoView.setMediaController(mc);

                                        if(i == 0){

                                            videoView1.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView1.setVideoURI(video);
                                            videoView1.start();

                                            link1 = LINK;
                                            width1 = WIDTH;
                                        }

                                        if(i == 2){
                                            videoView2.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView2.setVideoURI(video);
                                            videoView2.start();

                                            link2 = LINK;
                                            width2 = WIDTH;
                                        }


                                        if(i == 3){
                                            videoView3.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView3.setVideoURI(video);
                                            videoView3.start();
                                            link3 = LINK;
                                            width3 = WIDTH;
                                        }

                                        if(i == 4){
                                            videoView4.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView4.setVideoURI(video);
                                            videoView4.start();
                                            link4 = LINK;
                                            width4 = WIDTH;
                                        }

                                        if(i == 5){
                                            videoView5.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView5.setVideoURI(video);
                                            videoView5.start();
                                            link5 = LINK;
                                            width5 = WIDTH;
                                        }

                                        if(i == 6){
                                            videoView6.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView6.setVideoURI(video);
                                            videoView6.start();
                                            link6 = LINK;
                                            width6 = WIDTH;
                                        }

                                        if(i == 7){
                                            videoView7.getLayoutParams().height = Integer.parseInt(WIDTH);
                                            videoView7.setVideoURI(video);
                                            videoView7.start();
                                            link7 = LINK;
                                            width7 = WIDTH;
                                        }



                                        //}



                                        /*arrayContenido.add(objeto.getString("contenido"));
                                        arrayComentario.add(objeto.getString("cantidad_comentarios"));
                                        arrayLike.add(objeto.getString("likes"));
                                        arrayHoraPublicacion.add(objeto.getString("fecha_publicacion"));
                                        arrayTitulo.add(objeto.getString("titulo"));
                                        arrayIdPublicacion.add(objeto.getString("id_publicacion"));*/
                                    }

                                } catch (Exception e) {
                                    Log.e("ERROR","ERROR");
                                    e.printStackTrace();
                                    System.out.println( e.getMessage() );
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                System.out.println("AYURAAAAA!!!!!!!!!!");
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror",error.toString());

                                rq.stop();
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        rq.add(json2);
    }

    public void addComentario(String link, String width){
        String url = "http://www.circlelabs.cl/app/confeciones/addComentario.php";
        requestQueue = Volley.newRequestQueue(Seleccionar_gif.this);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id_publicacion",idPublicacion);
        params.put("id_usuario_activo",idUsuario);
        params.put("link",link);
        params.put("width",width);

        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                try {

                                    System.out.println("RESPONSE");
                                    System.out.println(response);
                                    JSONObject arrayResponse = response.getJSONObject("publicacion");

                                    System.out.println( arrayResponse.getString("resultado") );

                                    if(arrayResponse.getString("resultado").equals("ok") ){



                                        /**actualizar el textView de cantidad de comentarios**/

                                        Toast.makeText(Seleccionar_gif.this.getBaseContext(),"Comentario agregado",
                                                Toast.LENGTH_SHORT).show();

                                        onBackPressed();

                                    }else{
                                        Toast.makeText(Seleccionar_gif.this.getBaseContext(),"Error agregar comentario",
                                                Toast.LENGTH_SHORT).show();

                                    }

                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);
    }
}
