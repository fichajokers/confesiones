package com.circlelabs.app.confesiones;


/**
 * Created by Fabian on 24/06/2016.
 */
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Point;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class ListViewMisConfesiones extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> idPublicacion;
    private final ArrayList<String> titulo;
    private final ArrayList<String> categoria;

    public ListViewMisConfesiones(
            Activity context,
            ArrayList<String> idPublicacion,
            ArrayList<String> titulo,
            ArrayList<String> categoria) {
        super(context, R.layout.list_view_confesiones,idPublicacion);
        this.context = context;
        this.idPublicacion = idPublicacion;
        this.titulo = titulo;
        this.categoria = categoria;
    }

    TextView tvTitulo,tvCategoria;
    ImageView ivEliminar,ivVerMas;
    public View getView(final int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        final View rowView= inflater.inflate(R.layout.list_view_mis_confesiones, null, true);

        tvTitulo    = (TextView) rowView.findViewById(R.id.tvTitulo);
        tvCategoria = (TextView) rowView.findViewById(R.id.tvCategoria);

        ivVerMas    = (ImageView) rowView.findViewById(R.id.ivVerMas);
        ivEliminar = (ImageView) rowView.findViewById(R.id.ivEliminar);

        //asignar valores a los textos
        tvTitulo.setText(titulo.get(position));
        tvCategoria.setText(categoria.get(position));

        ivVerMas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Ver publicación",
                        Toast.LENGTH_SHORT).show();
                eliminarPublicacion();
            }
        });

        ivEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Eliminar publicación",
                        Toast.LENGTH_SHORT).show();
                verMas();
            }
        });

        return rowView;
    }

    public void eliminarPublicacion(){

    }

    public void verMas(){

    }
}