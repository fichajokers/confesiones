package com.circlelabs.app.confesiones;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Mis_confesiones extends AppCompatActivity {

    RequestQueue requestQueue;
    ProgressDialog progressDialog;
    String url,idPublicacion;
    Intent myIntent;

    ArrayList arrayIdPublicacion = new ArrayList<String>();
    ArrayList arrayTitulo = new ArrayList<String>();
    ArrayList arrayNombreGrupo = new ArrayList<String>();
    ArrayList arrayComentario = new ArrayList<String>();
    ArrayList arrayLike = new ArrayList<String>();
    ArrayList arrayHoraPublicacion = new ArrayList<String>();
    ArrayList arrayContenido = new ArrayList<String>();
    ArrayList arrayLink = new ArrayList<String>();
    ArrayList arrayWidth = new ArrayList<String>();

    ArrayAdapter arrayAdapterConfesiones;

    ListView listViewConfesiones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confesiones);

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setBackgroundDrawable( new ColorDrawable( Color.parseColor("#322f60") ) );

        myIntent = getIntent();
        idPublicacion = myIntent.getStringExtra("idPublicacion");

        url = "http://www.circlelabs.cl/app/confeciones/getPublicacionNotificacion.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id_publicacion", idPublicacion);

        Drawable man = getResources().getDrawable(R.drawable.relojarena);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminateDrawable(man);
        progressDialog.setMessage("Cargando confesion");
        progressDialog.show();

        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json1 = new JsonObjectRequest(url, parameters, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try{
                    progressDialog.dismiss();
                    System.out.println("RESPONSE CONFESION X");
                    JSONArray arrayResponse = response.getJSONArray("publicacion");



                    for(int i=0; i<arrayResponse.length(); i++){
                        JSONObject objeto = arrayResponse.getJSONObject(i);

                        arrayNombreGrupo.add(objeto.getString("categoria"));
                        arrayContenido.add(objeto.getString("contenido"));
                        arrayComentario.add(objeto.getString("cantidad_comentarios"));
                        arrayLike.add(objeto.getString("likes"));
                        arrayHoraPublicacion.add(objeto.getString("fecha_publicacion"));
                        arrayTitulo.add(objeto.getString("titulo"));
                        arrayIdPublicacion.add(objeto.getString("id_publicacion"));
                        arrayLink.add(objeto.getString("link"));
                        arrayWidth.add(objeto.getString("width"));
                    }

                    listViewConfesiones = (ListView) findViewById(R.id.contenedorListViewConfesion);
                    arrayAdapterConfesiones = new ListViewConfesiones(Mis_confesiones.this,arrayIdPublicacion,arrayTitulo,arrayNombreGrupo,arrayContenido,arrayComentario,arrayLike,arrayHoraPublicacion,arrayLink,arrayWidth);
                    listViewConfesiones.setAdapter(arrayAdapterConfesiones);

                }
                catch (Exception e){
                    Log.e("ERROR", "ERROR");
                    e.printStackTrace();
                    System.out.println(e.getMessage());
                    progressDialog.dismiss();
                    //mostrar el Layout de "no hay confesiones"
                    setContentView(R.layout.no_hay_publiaciones);
                }
            }


        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("VOLLEYeror", volleyError.toString());
                requestQueue.stop();
                progressDialog.dismiss();
                //mostrar el Layout de "no hay confesiones"
                setContentView(R.layout.no_hay_publiaciones);
            }
        }
        );

        json1.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json1);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
