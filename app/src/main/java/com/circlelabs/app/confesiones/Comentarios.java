package com.circlelabs.app.confesiones;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by matias on 16-08-17.
 */
public class Comentarios extends AppCompatActivity{
    ListView listView;
    SessionManager manager;
    Intent myIntent;
    String idUsuario,nombreUsuario,fotoUsuario,idPublicacion;
    RequestQueue requestQueue;
    int mDeviceHeight;
    ArrayAdapter arrayAdapterComentarios;
    ListView listViewComentarios;
    EditText etComentarioUsuario;
    ImageView btnGif,btnComentar;
    SwipeRefreshLayout swipeContainer;

    ArrayList arrayIdUsuario     = new ArrayList<String>();
    ArrayList arrayNombreUsuario = new ArrayList<String>();
    ArrayList arrayContenido     = new ArrayList<String>();
    ArrayList arrayFecha         = new ArrayList<String>();
    ArrayList arrayIdComentario  = new ArrayList<String>();
    ArrayList arrayFoto          = new ArrayList<String>();
    ArrayList arrayLink          = new ArrayList<String>();
    ArrayList arrayWidth         = new ArrayList<String>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fb_popup_layout);

        getSupportActionBar().hide();

        myIntent = getIntent();
        idPublicacion = myIntent.getStringExtra("idPublicacion");

        manager       = new SessionManager();
        idUsuario     = manager.getPreferences(Comentarios.this,"sesionUsuario");
        nombreUsuario = manager.getPreferences(Comentarios.this,"sesionNombreUsuario");
        fotoUsuario   = manager.getPreferences(Comentarios.this,"sesionFotoUsuario");


        listView            = (ListView) findViewById(R.id.commentsListView);
        etComentarioUsuario = (EditText) findViewById(R.id.etComentarioUsuario);
        btnComentar         = (ImageView) findViewById(R.id.btnComentar);
        btnGif              = (ImageView) findViewById(R.id.btnGif);

        setSimpleList(listView);

        // get device size
        Display display = Comentarios.this.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        mDeviceHeight = size.y;

        etComentarioUsuario.getLayoutParams().width= size.x - 100;


        etComentarioUsuario.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            private int lastFocussedPosition = -1;
            private int position = 0;
            private Handler handler = new Handler();
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    handler.postDelayed(new Runnable() {

                        @Override
                        public void run() {
                            if (lastFocussedPosition == -1 || lastFocussedPosition == position) {
                                lastFocussedPosition = position;
                                etComentarioUsuario.requestFocus();
                            }
                        }
                    }, 200);

                } else {
                    lastFocussedPosition = -1;
                }
            }
        });

        swipeContainer = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                arrayIdUsuario     = new ArrayList<String>();
                arrayNombreUsuario = new ArrayList<String>();
                arrayContenido     = new ArrayList<String>();
                arrayFecha         = new ArrayList<String>();
                arrayIdComentario  = new ArrayList<String>();
                arrayFoto          = new ArrayList<String>();
                arrayLink          = new ArrayList<String>();
                arrayWidth         = new ArrayList<String>();
                reloadComentarios();
            }
        });

        // Configure the refreshing colors
        swipeContainer.setColorSchemeResources(
                android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        btnGif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Comentarios.this, Seleccionar_gif.class);
                intent.putExtra("idPublicacion",idPublicacion);
                intent.putExtra("idUsuario",idUsuario);
                Comentarios.this.startActivity(intent);
            }
        });

        btnComentar.setOnClickListener(new View.OnClickListener() {

            String comentarioUsuario;
            @Override
            public void onClick(View v) {

                if(idUsuario.isEmpty()){
                    Toast.makeText(Comentarios.this,"Debes iniciar sesión para comentar",
                            Toast.LENGTH_SHORT).show();
                }else{
                    comentarioUsuario = etComentarioUsuario.getText().toString();
                    if( etComentarioUsuario.getText().toString().equals("") ){

                    }else{
                        etComentarioUsuario.setText("");
                        String url = "http://www.circlelabs.cl/app/confeciones/addComentario.php";
                        requestQueue = Volley.newRequestQueue(Comentarios.this);
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("id_publicacion",idPublicacion);
                        params.put("id_usuario_activo",idUsuario);
                        params.put("contenido",comentarioUsuario);


                        final JSONObject parameters = new JSONObject(params);

                        JsonObjectRequest json2 = new JsonObjectRequest
                                (url, parameters,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject response) {

                                                try {

                                                    System.out.println("RESPONSE");
                                                    System.out.println(response);
                                                    JSONObject arrayResponse = response.getJSONObject("publicacion");

                                                    System.out.println( arrayResponse.getString("resultado") );

                                                    if(arrayResponse.getString("resultado").equals("ok") ){

                                                        arrayIdUsuario.add( idUsuario );
                                                        arrayNombreUsuario.add( nombreUsuario );
                                                        arrayContenido.add( comentarioUsuario );
                                                        arrayFecha.add("ahora");
                                                        arrayIdComentario.add( "1" );
                                                        arrayFoto.add(fotoUsuario);
                                                        arrayLink.add("");
                                                        arrayWidth.add("");
                                                        arrayAdapterComentarios.notifyDataSetChanged();

                                                        etComentarioUsuario.setText("");

                                                        /**actualizar el textView de cantidad de comentarios**/
                                                        /*int cantComentarios = Integer.valueOf(comentarios.get(position));
                                                        System.out.println("[--------cantidad comentarios------]" + cantComentarios);
                                                        comentarios.set(position,Integer.toString( cantComentarios + 1));
                                                        tvCantidadComentario.setText( comentarios.get(position) );
                                                        notifyDataSetChanged();*/

                                                        /**actualizar el textView de cantidad de comentarios**/

                                                        Toast.makeText(Comentarios.this,"Comentario agregado",
                                                                Toast.LENGTH_SHORT).show();

                                                    }else{
                                                        Toast.makeText(Comentarios.this,"Error agregar comentario",
                                                                Toast.LENGTH_SHORT).show();

                                                    }

                                                } catch (Exception e) {
                                                    //loading.dismiss();
                                                    Log.e("ERROR", "ERROR");
                                                    e.printStackTrace();
                                                    System.out.println(e.getMessage());
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                // TODO Auto-generated method stub
                                                Log.e("VOLLEYeror", error.toString());
                                                requestQueue.stop();
                                            }
                                        }
                                );
                        json2.setRetryPolicy(new DefaultRetryPolicy(
                                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                        ));
                        requestQueue.add(json2);
                    }
                }
            }
        });
    }


    void setSimpleList(final ListView listView){

        String url = "http://www.circlelabs.cl/app/confeciones/getComentario.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id_publicacion", idPublicacion);

        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    System.out.println("RESPONSE");
                                    System.out.println(response);
                                    JSONArray arrayResponse = response.getJSONArray("publicacion");
                                    //System.out.println(arrayResponse);

                                    for(int i=0; i<arrayResponse.length(); i++){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);

                                        arrayIdComentario.add(objeto.getString("id_comentario"));
                                        arrayIdUsuario.add(objeto.getString("id_usuario"));
                                        arrayNombreUsuario.add(objeto.getString("nombreUsuario"));
                                        arrayContenido.add(objeto.getString("contenido"));
                                        arrayFecha.add(objeto.getString("fecha"));
                                        arrayFoto.add(objeto.getString("foto"));
                                        arrayLink.add(objeto.getString("link"));
                                        arrayWidth.add(objeto.getString("width"));
                                    }

                                    listViewComentarios = (ListView) listView.findViewById(R.id.commentsListView);
                                    arrayAdapterComentarios = new ListViewComentarios(Comentarios.this,arrayIdComentario,arrayIdUsuario,arrayNombreUsuario,arrayContenido,arrayFecha,arrayFoto,arrayLink,arrayWidth);
                                    listViewComentarios.setAdapter(arrayAdapterComentarios);

                                    final RelativeLayout relativeCargando = (RelativeLayout) findViewById(R.id.cargando);
                                    relativeCargando.setVisibility(View.GONE);

                                    //System.out.println("[][][][][[][][][][][[]-----------__> " + arrayFoto);

                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);
    }

    public void reloadComentarios(){
        String url = "http://www.circlelabs.cl/app/confeciones/getComentario.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id_publicacion", idPublicacion);

        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    System.out.println("RESPONSE");
                                    System.out.println(response);
                                    JSONArray arrayResponse = response.getJSONArray("publicacion");
                                    //System.out.println(arrayResponse);

                                    for(int i=0; i<arrayResponse.length(); i++){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);

                                        arrayIdComentario.add(objeto.getString("id_comentario"));
                                        arrayIdUsuario.add(objeto.getString("id_usuario"));
                                        arrayNombreUsuario.add(objeto.getString("nombreUsuario"));
                                        arrayContenido.add(objeto.getString("contenido"));
                                        arrayFecha.add(objeto.getString("fecha"));
                                        arrayFoto.add(objeto.getString("foto"));
                                        arrayLink.add(objeto.getString("link"));
                                        arrayWidth.add(objeto.getString("width"));
                                    }

                                    listViewComentarios = (ListView) listView.findViewById(R.id.commentsListView);
                                    arrayAdapterComentarios = new ListViewComentarios(Comentarios.this,arrayIdComentario,arrayIdUsuario,arrayNombreUsuario,arrayContenido,arrayFecha,arrayFoto,arrayLink,arrayWidth);
                                    listViewComentarios.setAdapter(arrayAdapterComentarios);

                                    final RelativeLayout relativeCargando = (RelativeLayout) findViewById(R.id.cargando);
                                    relativeCargando.setVisibility(View.GONE);

                                    swipeContainer.setRefreshing(false);
                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                    swipeContainer.setRefreshing(false);
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                                swipeContainer.setRefreshing(false);
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("VOLVI DE SELECIONAR GIFFFFF");

        arrayIdUsuario     = new ArrayList<String>();
        arrayNombreUsuario = new ArrayList<String>();
        arrayContenido     = new ArrayList<String>();
        arrayFecha         = new ArrayList<String>();
        arrayIdComentario  = new ArrayList<String>();
        arrayFoto          = new ArrayList<String>();
        arrayLink          = new ArrayList<String>();
        arrayWidth         = new ArrayList<String>();
        reloadComentarios();
    }

}
