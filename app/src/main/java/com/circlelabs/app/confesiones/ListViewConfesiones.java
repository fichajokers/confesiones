package com.circlelabs.app.confesiones;


/**
 * Created by Fabian on 24/06/2016.
 */
        import android.app.Activity;
import android.app.ProgressDialog;
        import android.content.Context;
        import android.content.Intent;
        import android.content.res.Configuration;
        import android.graphics.Point;
        import android.graphics.Rect;
        import android.graphics.drawable.Drawable;
        import android.inputmethodservice.KeyboardView;
        import android.media.MediaPlayer;
        import android.net.Uri;
        import android.text.InputType;
        import android.util.DisplayMetrics;
        import android.util.Log;
        import android.view.Display;
        import android.view.Gravity;
        import android.view.KeyEvent;
        import android.view.LayoutInflater;
        import android.view.MotionEvent;
        import android.view.View;
import android.view.ViewGroup;
        import android.view.WindowManager;
        import android.view.inputmethod.InputMethodManager;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.EditText;
        import android.widget.HorizontalScrollView;
        import android.widget.ImageView;
        import android.widget.LinearLayout;
        import android.widget.ListView;
        import android.widget.PopupWindow;
        import android.widget.RelativeLayout;
        import android.widget.ScrollView;
        import android.widget.TextView;
        import android.widget.Toast;
        import android.widget.VideoView;

        import com.android.volley.DefaultRetryPolicy;
        import com.android.volley.RequestQueue;
        import com.android.volley.Response;
        import com.android.volley.VolleyError;
        import com.android.volley.toolbox.ImageLoader;
        import com.android.volley.toolbox.JsonObjectRequest;
        import com.android.volley.toolbox.NetworkImageView;
        import com.android.volley.toolbox.Volley;
        import com.danikula.videocache.HttpProxyCacheServer;
        import com.ms.square.android.expandabletextview.ExpandableTextView;

        import org.json.JSONArray;
        import org.json.JSONObject;

        import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
        import java.util.Calendar;
        import java.util.HashMap;

public class ListViewConfesiones extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<String> idPublicacion;
    private final ArrayList<String> titulo;
    private final ArrayList<String> categoria;
    private final ArrayList<String> contenido;
    private final ArrayList<String> comentarios;
    private final ArrayList<String> likes;
    private final ArrayList<String> fecha_publicacion;
    private final ArrayList<String> link;
    private final ArrayList<String> width;


    public ListViewConfesiones(
            Activity context,
            ArrayList<String> idPublicacion,
            ArrayList<String> titulo,
            ArrayList<String> categoria,
            ArrayList<String> contenido,
            ArrayList<String> comentarios,
            ArrayList<String> likes,
            ArrayList<String> fecha_publicacion,
            ArrayList<String> link,
            ArrayList<String> width) {
        super(context, R.layout.list_view_confesiones,idPublicacion);
        this.context = context;
        this.idPublicacion = idPublicacion;
        this.titulo = titulo;
        this.categoria = categoria;
        this.contenido = contenido;
        this.comentarios = comentarios;
        this.likes = likes;
        this.fecha_publicacion = fecha_publicacion;
        this.link = link;
        this.width = width;
    }

    TextView tvTitulo,tvNombreGrupo,expandable_text,tvCantidadComentario,tvCantidadLike,tvFechaPublicacion;
    ImageView ivComentarios,ivCantLike,ivShare,cargando;
    PopupWindow popWindow,popWindowGif;
    int mDeviceHeight;
    RequestQueue requestQueue;
    ListView listViewComentarios;
    ArrayAdapter arrayAdapterComentarios;
    VideoView gifSeleccionado;
    LinearLayout contenedorGifSeleccionado;


    ArrayList arrayIdUsuario     = new ArrayList<String>();
    ArrayList arrayNombreUsuario = new ArrayList<String>();
    ArrayList arrayContenido     = new ArrayList<String>();
    ArrayList arrayFecha         = new ArrayList<String>();
    ArrayList arrayIdComentario  = new ArrayList<String>();
    ArrayList arrayFoto          = new ArrayList<String>();
    ArrayList arrayLink          = new ArrayList<String>();
    ArrayList arrayWidth         = new ArrayList<String>();


    ProgressDialog progressDialog;
    SessionManager manager;
    String idUsuario,nombreUsuario,fotoUsuario;


    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        manager       = new SessionManager();
        idUsuario     = manager.getPreferences(context,"sesionUsuario");
        nombreUsuario = manager.getPreferences(context,"sesionNombreUsuario");
        fotoUsuario   = manager.getPreferences(context,"sesionFotoUsuario");

        LayoutInflater inflater = context.getLayoutInflater();
        final View rowView= inflater.inflate(R.layout.list_view_confesiones, null, true);

        tvTitulo             = (TextView) rowView.findViewById(R.id.tvTitulo);
        tvNombreGrupo        = (TextView) rowView.findViewById(R.id.tvNombreGrupo);
        expandable_text      = (TextView) rowView.findViewById(R.id.expandable_text);
        tvCantidadComentario = (TextView) rowView.findViewById(R.id.tvCantidadComentario);
        tvCantidadLike       = (TextView) rowView.findViewById(R.id.tvCantidadLike);
        tvFechaPublicacion   = (TextView) rowView.findViewById(R.id.tvFechaPublicacion);

        ivComentarios        = (ImageView) rowView.findViewById(R.id.ivComentarios);
        ivCantLike           = (ImageView) rowView.findViewById(R.id.ivCantLike);
        ivShare              = (ImageView) rowView.findViewById(R.id.ivShare);

        gifSeleccionado      = (VideoView) rowView.findViewById(R.id.gifSeleccionado);
        contenedorGifSeleccionado = (LinearLayout) rowView.findViewById(R.id.contenedorGifSeleccionado);

        //asignar gif si existe el link
        if( !link.get(position).equals("")  ){
            //System.out.println( "Deberia entrar una sola vezs " );
            contenedorGifSeleccionado.setVisibility(View.VISIBLE);

            //Uri video = Uri.parse(link.get(position));
            //gifSeleccionado.setVideoURI(video);

            HttpProxyCacheServer proxy = ProxyFactory.getProxy(context);
            String proxyUrl = proxy.getProxyUrl(link.get(position));
            gifSeleccionado.setVideoPath(proxyUrl);

            if( !width.get(position).equals("") )
                gifSeleccionado.getLayoutParams().height = Integer.parseInt(  width.get(position).toString() );


            gifSeleccionado.start();

        }
        gifSeleccionado.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setLooping(true);
            }
        });

        //asignar valores a los textos

        tvTitulo.setText(titulo.get(position));
        tvNombreGrupo.setText(categoria.get(position));
        tvCantidadComentario.setText(comentarios.get(position));
        tvCantidadLike.setText(likes.get(position));
        tvFechaPublicacion.setText(fecha_publicacion.get(position));

        ExpandableTextView expandableTextView = (ExpandableTextView) rowView.findViewById(R.id.expand_text_view);
        expandableTextView.setText(contenido.get(position));

        ivComentarios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arrayIdUsuario       = new ArrayList<String>();
                arrayNombreUsuario   = new ArrayList<String>();
                arrayContenido       = new ArrayList<String>();
                arrayFecha           = new ArrayList<String>();
                arrayIdComentario    = new ArrayList<String>();
                arrayFoto            = new ArrayList<String>();
                arrayLink  = new ArrayList<String>();
                arrayWidth = new ArrayList<String>();
                onShowPopup(v, position);

                Intent intent = new Intent(context, Comentarios.class);
                intent.putExtra("idPublicacion",idPublicacion.get(position));
                context.startActivity(intent);
            }
        });

        ivCantLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addLike(idPublicacion.get(position), idUsuario, position);
            }
        });

        ivShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Drawable man = context.getResources().getDrawable(R.drawable.relojarena);
                progressDialog = new ProgressDialog(context);
                progressDialog.setCanceledOnTouchOutside(false);
                progressDialog.setIndeterminateDrawable(man);
                progressDialog.setMessage("Arreglando confesión");
                progressDialog.show();

                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                String longUrl1 = "www.circlelabs.cl/confesiones/share/index.php?id="+idPublicacion.get(position)+"&imagen=http://www.circlelabs.cl/confesiones/share/icono.png";
                ShortUrl.makeShortUrl(longUrl1, new ShortUrl.ShortUrlListener() {
                    @Override
                    public void OnFinish(String url) {

                        if (url != null && 0 < url.length()) {
                            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                            sharingIntent.setType("text/plain");
                            String shareBody = url;
                            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
                            sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                            getContext().startActivity(Intent.createChooser(sharingIntent, "Compartir"));
                            progressDialog.dismiss();

                        } else {
                            System.out.println("ERROR");
                        }

                    }
                });

            }
        });

        return rowView;
    }

    public static String shorten(String longUrl) {

        if (longUrl == null) {
            return longUrl;
        }
        StringBuilder sb = null;
        String line = null;
        String urlStr = longUrl;

        try {
            URL url = new URL("http://goo.gl/api/url");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("User-Agent", "toolbar");

            OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
            writer.write("url=" + URLEncoder.encode(urlStr, "UTF-8"));
            writer.close();

            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            sb = new StringBuilder();
            while ((line = rd.readLine()) != null) {
                sb.append(line + '\n');
            }

            String json = sb.toString();
            //It extracts easily...
            return json.substring(json.indexOf("http"), json.indexOf("\"", json.indexOf("http")));
        } catch (MalformedURLException e) {
            return longUrl;
        } catch (IOException e) {
            return longUrl;
        }
    }

    // call this method when required to show popup
    public void onShowPopup(View v, Integer position){

        /*LayoutInflater layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        final View inflatedView = layoutInflater.inflate(R.layout.fb_popup_layout, null,false);
        // find the ListView in the popup layout
        ListView listView = (ListView)inflatedView.findViewById(R.id.commentsListView);

        accionesVistaComentarios(inflatedView,position);


        // get device size
        Display display = context.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        mDeviceHeight = size.y;

        // fill the data to the list items
        setSimpleList(listView,position,inflatedView);

        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);

        // set a background drawable with rounders corners
        popWindow.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.fb_popup_bg));
        // make it focusable to show the keyboard to enter in `EditText`
        popWindow.setFocusable(true);
        // make it outside touchable to dismiss the popup window
        popWindow.setOutsideTouchable(true);
        //set animation
        popWindow.setAnimationStyle(R.style.DialogAnimation_2);
        // show the popup at center of the screen and set some margin at bottom ie,
        popWindow.showAtLocation(v, Gravity.TOP, 0,0);*/
    }


    void setSimpleList(final ListView listView, Integer position,final View inflated2View){

        String url = "http://www.circlelabs.cl/app/confeciones/getComentario.php";
        requestQueue = Volley.newRequestQueue(context);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("id_publicacion", idPublicacion.get(position));

        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    System.out.println("RESPONSE");
                                    System.out.println(response);
                                    JSONArray arrayResponse = response.getJSONArray("publicacion");
                                    //System.out.println(arrayResponse);

                                    for(int i=0; i<arrayResponse.length(); i++){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);

                                        arrayIdComentario.add(objeto.getString("id_comentario"));
                                        arrayIdUsuario.add(objeto.getString("id_usuario"));
                                        arrayNombreUsuario.add(objeto.getString("nombreUsuario"));
                                        arrayContenido.add(objeto.getString("contenido"));
                                        arrayFecha.add(objeto.getString("fecha"));
                                        arrayFoto.add(objeto.getString("foto"));
                                        arrayLink.add(objeto.getString("link"));
                                        arrayWidth.add(objeto.getString("width"));
                                    }

                                    listViewComentarios = (ListView) listView.findViewById(R.id.commentsListView);
                                    arrayAdapterComentarios = new ListViewComentarios(context,arrayIdComentario,arrayIdUsuario,arrayNombreUsuario,arrayContenido,arrayFecha,arrayFoto,arrayLink,arrayWidth);
                                    listViewComentarios.setAdapter(arrayAdapterComentarios);

                                    final RelativeLayout relativeCargando = (RelativeLayout) inflated2View.findViewById(R.id.cargando);
                                    relativeCargando.setVisibility(View.GONE);

                                    //System.out.println("[][][][][[][][][][][[]-----------__> " + arrayFoto);

                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);
    }

    public void addLike(String publicacion, String idUsuario,final int position){

        if(idUsuario.isEmpty()){
            Toast.makeText(context.getBaseContext(),"Debes iniciar sesión para dar Like",
                    Toast.LENGTH_SHORT).show();
        }else{
            String url = "http://www.circlelabs.cl/app/confeciones/addLike.php";
            requestQueue = Volley.newRequestQueue(context);
            HashMap<String, String> params = new HashMap<String, String>();
            params.put("id_publicacion", publicacion);
            params.put("id_usuario_activo",idUsuario);


            final JSONObject parameters = new JSONObject(params);

            JsonObjectRequest json1 = new JsonObjectRequest
                    (url, parameters,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {

                                    try {
                                        System.out.println("RESPONSE");
                                        System.out.println(response);

                                        JSONObject objeto = response.getJSONObject("publicacion");
                                        System.out.println(objeto.getString("resultado"));

                                        if(objeto.getString("resultado").equals("ok")){
                                            int cantidad = new Integer(  likes.get(position) ) + 1;

                                            likes.set(position,Integer.toString(cantidad));
                                            tvCantidadLike.setText( likes.get(position) );
                                            notifyDataSetChanged();
                                        }else{

                                        }





                                    } catch (Exception e) {
                                        //loading.dismiss();
                                        Log.e("ERROR", "ERROR");
                                        e.printStackTrace();
                                        System.out.println(e.getMessage());
                                        progressDialog.hide();
                                    }

                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    // TODO Auto-generated method stub
                                    Log.e("VOLLEYeror", error.toString());
                                    requestQueue.stop();
                                    progressDialog.hide();
                                }
                            }
                    );
            json1.setRetryPolicy(new DefaultRetryPolicy(
                    5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));
            requestQueue.add(json1);
        }
    }

    public void accionesVistaComentarios(final View inflatedView, final int position){
        //almacenar el nuevo comentario del usuario.
        final TextView etComentarioUsuario = (TextView) inflatedView.findViewById(R.id.etComentarioUsuario);
        final ImageView btnComentar        = (ImageView) inflatedView.findViewById(R.id.btnComentar);
        final ImageView btnGif             = (ImageView) inflatedView.findViewById(R.id.btnGif);

        // get device size
        Display display = context.getWindowManager().getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
        mDeviceHeight = size.y;

        etComentarioUsuario.getLayoutParams().width= size.x - 100;

        btnGif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("AUTORIDAD!");
                seleccionarGif(idPublicacion.get(position),idUsuario);
            }
        });
        btnComentar.setOnClickListener(new View.OnClickListener() {

            String comentarioUsuario;
            @Override
            public void onClick(View v) {

                if(idUsuario.isEmpty()){
                    Toast.makeText(context.getBaseContext(),"Debes iniciar sesión para comentar",
                            Toast.LENGTH_SHORT).show();
                }else{
                    comentarioUsuario = etComentarioUsuario.getText().toString();
                    if( etComentarioUsuario.getText().toString().equals("") ){

                    }else{
                        etComentarioUsuario.setText("");
                        String url = "http://www.circlelabs.cl/app/confeciones/addComentario.php";
                        requestQueue = Volley.newRequestQueue(context);
                        HashMap<String, String> params = new HashMap<String, String>();
                        params.put("id_publicacion",idPublicacion.get(position));
                        params.put("id_usuario_activo",idUsuario);
                        params.put("contenido",comentarioUsuario);


                        final JSONObject parameters = new JSONObject(params);

                        JsonObjectRequest json2 = new JsonObjectRequest
                                (url, parameters,
                                        new Response.Listener<JSONObject>() {
                                            @Override
                                            public void onResponse(JSONObject response) {

                                                try {

                                                    System.out.println("RESPONSE");
                                                    System.out.println(response);
                                                    JSONObject arrayResponse = response.getJSONObject("publicacion");

                                                    System.out.println( arrayResponse.getString("resultado") );

                                                    if(arrayResponse.getString("resultado").equals("ok") ){

                                                        arrayIdUsuario.add( idUsuario );
                                                        arrayNombreUsuario.add( nombreUsuario );
                                                        arrayContenido.add( comentarioUsuario );
                                                        arrayFecha.add("ahora");
                                                        arrayIdComentario.add( "1" );
                                                        arrayFoto.add(fotoUsuario);
                                                        arrayAdapterComentarios.notifyDataSetChanged();

                                                        etComentarioUsuario.setText("");

                                                        /**actualizar el textView de cantidad de comentarios**/
                                                        int cantComentarios = Integer.valueOf(comentarios.get(position));
                                                        System.out.println("[--------cantidad comentarios------]" + cantComentarios);
                                                        comentarios.set(position,Integer.toString( cantComentarios + 1));
                                                        tvCantidadComentario.setText( comentarios.get(position) );
                                                        notifyDataSetChanged();

                                                        /**actualizar el textView de cantidad de comentarios**/

                                                        Toast.makeText(context.getBaseContext(),"Comentario agregado",
                                                                Toast.LENGTH_SHORT).show();

                                                    }else{
                                                        Toast.makeText(context.getBaseContext(),"Error agregar comentario",
                                                                Toast.LENGTH_SHORT).show();

                                                    }

                                                } catch (Exception e) {
                                                    //loading.dismiss();
                                                    Log.e("ERROR", "ERROR");
                                                    e.printStackTrace();
                                                    System.out.println(e.getMessage());
                                                }
                                            }
                                        },
                                        new Response.ErrorListener() {
                                            @Override
                                            public void onErrorResponse(VolleyError error) {
                                                // TODO Auto-generated method stub
                                                Log.e("VOLLEYeror", error.toString());
                                                requestQueue.stop();
                                            }
                                        }
                                );
                        json2.setRetryPolicy(new DefaultRetryPolicy(
                                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                        ));
                        requestQueue.add(json2);
                    }
                }
            }
        });

    }

    public void seleccionarGif(String idPublicacion, String idUsuario){
        Intent intent = new Intent(context, Seleccionar_gif.class);
        intent.putExtra("idPublicacion",idPublicacion);
        intent.putExtra("idUsuario",idUsuario);
        context.startActivity(intent);
    }

    public void accionesPopupSeleciconarGif(final View inflatedView, final int position){


    }
}

