package com.circlelabs.app.confesiones;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import android.preference.PreferenceManager;

import org.json.JSONObject;

import java.util.HashMap;

public class Login extends AppCompatActivity {

    Button btLogin;
    EditText tvUsuario;
    EditText tvClave;

    ProgressDialog progressDialog;
    RequestQueue rq;

    SessionManager manager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        //cambiar nombre del action bar
        getSupportActionBar().setTitle("Inicio Sesión");
        getSupportActionBar().setElevation(0);
        //Habilitar la flechita para atras
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable( new ColorDrawable( Color.parseColor("#322f60") ) );

        btLogin = (Button) findViewById(R.id.btLogin);
        tvUsuario = (EditText) findViewById(R.id.tvUsuario);
        tvClave = (EditText) findViewById(R.id.tvClave);

        Drawable man = getResources().getDrawable(R.drawable.relojarena);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminateDrawable(man);
        progressDialog.setMessage("Iniciando sesión");

        manager = new SessionManager();
        String sesionUsuario = manager.getPreferences(Login.this, "sesionUsuario");


        btLogin.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {


                progressDialog.show();
                rq = Volley.newRequestQueue(getApplicationContext());

                HashMap<String, String> params2 = new HashMap<String, String>();
                params2.put("usuario",tvUsuario.getText().toString());
                params2.put("clave", tvClave.getText().toString());


                final JSONObject parameters = new JSONObject(params2);
                JsonObjectRequest json2 = new JsonObjectRequest
                        ("http://www.circlelabs.cl/app/confeciones/login.php", parameters,
                                new Response.Listener<JSONObject>() {
                                    @Override
                                    public void onResponse(JSONObject response7) {
                                        try {
                                            progressDialog.show();
                                            System.out.println(response7);
                                            JSONObject resultado = response7.getJSONObject("publicacion");
                                            String res = resultado.getString("resultado");

                                            System.out.println("REPSONSE LOGIN");
                                            System.out.println(resultado);

                                            if(res.equals("ok")){
                                                String idUsuario = resultado.getString("idUsuario");
                                                String nombreUsuario = resultado.getString("nombre");
                                                String fotoUsuario = resultado.getString("fotoUsuario");
                                                System.out.println(idUsuario);
                                                manager.setPreferences(Login.this, "sesionUsuario", idUsuario);
                                                manager.setPreferences(Login.this, "sesionNombreUsuario",nombreUsuario);
                                                manager.setPreferences(Login.this, "sesionFotoUsuario", fotoUsuario);


                                                progressDialog.hide();
                                                //getCategorias();
                                                Intent nextPage = new Intent(getApplicationContext(),MainActivity.class);
                                                startActivity(nextPage);
                                            }
                                            else
                                            {
                                                progressDialog.hide();
                                                Toast.makeText(getApplicationContext(),"Usuario o contraseña incorrecta, intente nuevamente", Toast.LENGTH_SHORT).show();

                                            }

                                        } catch (Exception e) {
                                            //loading.dismiss();
                                            progressDialog.hide();

                                            Log.e("ERROR","ERROR");
                                            e.printStackTrace();
                                            System.out.println( e.getMessage() );
                                        }

                                    }
                                },
                                new Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        System.out.println("AYURAAAAA!!!!!!!!!!");
                                        // TODO Auto-generated method stub
                                        Log.e("VOLLEYeror",error.toString());
                                        progressDialog.show();

                                        rq.stop();
                                    }
                                }
                        );
                json2.setRetryPolicy(new DefaultRetryPolicy(
                        5000,DefaultRetryPolicy.DEFAULT_MAX_RETRIES,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                ));
                rq.add(json2);

            }
        });


    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
