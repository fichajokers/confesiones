package com.circlelabs.app.confesiones;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class Nuevo_grupo extends AppCompatActivity {
    EditText etNombreGrupo;
    Spinner acNombreRegion;
    Button btnCreargrupo;
    RequestQueue requestQueue;
    ArrayList region = new ArrayList<String>();
    ArrayList idRegion = new ArrayList<String>();
    ProgressDialog progressDialog;
    String nombreRegion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nuevo_grupo);
        //cambiar nombre del action bar
        getSupportActionBar().setTitle("Nuevo grupo");
        getSupportActionBar().setElevation(0);
        //Habilitar la flechita para atras
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable( new ColorDrawable( Color.parseColor("#322f60") ) );

        Drawable tiempo = getResources().getDrawable(R.drawable.relojarena);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminateDrawable(tiempo);
        progressDialog.setMessage("Creando grupo");

        acNombreRegion = (Spinner) findViewById(R.id.acNombreRegion);
        etNombreGrupo  = (EditText) findViewById(R.id.etNombreGrupo);

        acNombreRegion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                nombreRegion =  parent.getItemAtPosition(position).toString();
                //System.out.println("[sdffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff] " + idNombreRegion + " ----- " + id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        llenarAutocompleteRegiones(acNombreRegion);

        btnCreargrupo = (Button) findViewById(R.id.btnCreargrupo);

        btnCreargrupo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String grupo;
                grupo  = etNombreGrupo.getText().toString();
                /*Toast.makeText(Nuevo_grupo.this, grupo+ " | " + region,
                        Toast.LENGTH_SHORT).show();*/
                crearGrupo(grupo,nombreRegion);
            }
        });

    }

    public void crearGrupo(String grupo, final String regionSeleccionada){
        progressDialog.show();
        region.indexOf(region);
        System.out.println("[]               ------> " +region.indexOf(regionSeleccionada)  );
        System.out.println("[]               ------> " +idRegion.get( region.indexOf(regionSeleccionada) )  );
        String idRegionSeleccionada = idRegion.get( region.indexOf(regionSeleccionada)).toString();

        String url = "http://www.circlelabs.cl/app/confeciones/addGrupo.php";
        requestQueue = Volley.newRequestQueue(this);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("nombre_grupo", grupo);
        params.put("id_region", idRegionSeleccionada );
        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    final ProgressDialog cancelDialog;
                                    progressDialog.dismiss();
                                    System.out.println(response);
                                    JSONObject arrayResponse = response.getJSONObject("publicacion");

                                    if(arrayResponse.getString("resultado").equals("ok") ){
                                        //acNombreRegion.setText("");
                                        etNombreGrupo.setText("");

                                        Drawable nointernet = getResources().getDrawable(R.drawable.clover);
                                        cancelDialog = new ProgressDialog(Nuevo_grupo.this);
                                        cancelDialog.setCanceledOnTouchOutside(false);
                                        cancelDialog.setIndeterminateDrawable(nointernet);
                                        cancelDialog.setMessage("Grupo creado correctamente");
                                        cancelDialog.setButton("Entendido", new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(DialogInterface dialog, int which)
                                            {
                                                // Use either finish() or return() to either close the activity or just the dialog
                                                cancelDialog.dismiss();
                                                return;
                                            }
                                        });
                                        cancelDialog.show();
                                    }else{
                                        Drawable nointernet = getResources().getDrawable(R.drawable.delete);
                                        cancelDialog = new ProgressDialog(Nuevo_grupo.this);
                                        cancelDialog.setCanceledOnTouchOutside(false);
                                        cancelDialog.setIndeterminateDrawable(nointernet);
                                        cancelDialog.setMessage("Error al crear grupo");
                                        cancelDialog.setButton("Entendido", new DialogInterface.OnClickListener()
                                        {
                                            public void onClick(DialogInterface dialog, int which)
                                            {
                                                // Use either finish() or return() to either close the activity or just the dialog
                                                cancelDialog.dismiss();
                                                return;
                                            }
                                        });
                                        cancelDialog.show();
                                    }

                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);
    }

    public void llenarAutocompleteRegiones(final Spinner acNombreRegion){
        String url = "http://www.circlelabs.cl/app/confeciones/getRegiones.php";

        requestQueue = Volley.newRequestQueue(this);
        HashMap<String, String> params = new HashMap<String, String>();
        final JSONObject parameters = new JSONObject(params);

        JsonObjectRequest json2 = new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    //System.out.println("RESPONSE");
                                    //System.out.println(response);
                                    JSONArray arrayResponse = response.getJSONArray("regiones");
                                    System.out.println(arrayResponse);

                                    for(int i=0; i<arrayResponse.length(); i++){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);
                                        //System.out.println(objeto.getString("nombre"));
                                        region.add(objeto.getString("nombre"));
                                        idRegion.add(objeto.getString("idRegion"));
                                    }

                                    ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                            (Nuevo_grupo.this,android.R.layout.select_dialog_item, region);
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                    acNombreRegion.setAdapter(adapter);

                                    /*ArrayAdapter<String> adapter = new ArrayAdapter<String>
                                        (Nuevo_grupo.this,android.R.layout.select_dialog_item, region);

                                    acNombreRegion.setThreshold(2);
                                    acNombreRegion.setAdapter(adapter);*/
                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                            }
                        }
                );
        json2.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json2);
    }
}
