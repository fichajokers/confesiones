package com.circlelabs.app.confesiones;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class VerMasCategoria extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "";
    RequestQueue requestQueue;
    ProgressDialog progressDialog;
    String url;


    ArrayList arrayNombreGrupo = new ArrayList<String>();
    ArrayList arrayIdCategoria = new ArrayList<String>();
    ArrayList arrayfoto        = new ArrayList<String>();

    ArrayAdapter arrayAdapterCategorias;

    ListView listViewCategorias;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_mas_categoria);

        //cambiar nombre del action bar
        getSupportActionBar().setTitle("Mas Grupos");
        getSupportActionBar().setElevation(0);
        //Habilitar la flechita para atras
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setBackgroundDrawable( new ColorDrawable( Color.parseColor("#322f60") ) );

        url = "http://www.circlelabs.cl/app/confeciones/getCategorias.php";
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        HashMap<String, String> params = new HashMap<String, String>();
        JSONObject parameters = new JSONObject(params);
        //params.put("id_publicacion", "1");

        Drawable man = getResources().getDrawable(R.drawable.relojarena);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setIndeterminateDrawable(man);
        progressDialog.setMessage("Cargando grupos");
        progressDialog.show();

        JsonObjectRequest json1= new JsonObjectRequest
                (url, parameters,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    //System.out.println("CATEGORIAS");
                                    //System.out.println(response);
                                    JSONArray arrayResponse = response.getJSONArray("grupos");
                                    //System.out.println(arrayResponse);


                                    for(int i=0; i<arrayResponse.length(); i++){
                                        JSONObject objeto = arrayResponse.getJSONObject(i);

                                        arrayNombreGrupo.add(objeto.getString("nombre"));
                                        arrayIdCategoria.add(objeto.getString("idGrupo"));
                                        arrayfoto.add(objeto.getString("foto"));
                                    }


                                    listViewCategorias = (ListView) findViewById(R.id.contenedorListViewCategorias);
                                    arrayAdapterCategorias = new ListViewCategorias(VerMasCategoria.this, arrayIdCategoria,arrayNombreGrupo,arrayfoto);
                                    listViewCategorias.setAdapter(arrayAdapterCategorias);

                                    progressDialog.dismiss();

                                    listViewCategorias.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                            TextView v=(TextView) view.findViewById(R.id.tvNombreGrupo);
                                            TextView v1=(TextView) view.findViewById(R.id.tvIdCategoria);

                                            //id categoria de la fila seleccionada
                                            System.out.println("GET ETXT" + arrayIdCategoria.get(position));
                                            Intent intent = new Intent(getApplicationContext(), Confesiones.class);
                                            String idCategoria = String.valueOf(arrayIdCategoria.get(position));
                                            intent.putExtra(EXTRA_MESSAGE, idCategoria);
                                            intent.putExtra("NombreGrupoParam", String.valueOf(arrayNombreGrupo.get(position)));
                                            startActivity(intent);
                                        }
                                    });


                                } catch (Exception e) {
                                    //loading.dismiss();
                                    Log.e("ERROR", "ERROR");
                                    e.printStackTrace();
                                    System.out.println(e.getMessage());
                                    progressDialog.dismiss();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO Auto-generated method stub
                                Log.e("VOLLEYeror", error.toString());
                                requestQueue.stop();
                                progressDialog.dismiss();
                            }
                        }
                );
        json1.setRetryPolicy(new DefaultRetryPolicy(
                5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
        requestQueue.add(json1);
    }
}
