package com.circlelabs.app.confesiones;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Map;
import java.util.Random;

/**
 * Created by matias on 20-06-17.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        //Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());

        if (remoteMessage.getData().size() > 0) {
            try {
                JSONObject jsonObject = new JSONObject(remoteMessage.getData());
                Log.e("Tag",remoteMessage.getData().toString());

                Map<String, String> data = remoteMessage.getData();
                String id_publicacion = data.get("id_publicacion");
                String body  = data.get("body");
                String title = data.get("title");

                sendNotification(id_publicacion,body,title);
            } catch (Exception e) {

            }
        }
    }

    private void sendNotification(String msg,String body,String title) {
        Intent intent = new Intent(this, Mis_confesiones.class);
        intent.putExtra("idPublicacion",msg);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, new Random().nextInt(100) , intent,
                PendingIntent.FLAG_ONE_SHOT);
        long when = System.currentTimeMillis();
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(this);
        mNotifyBuilder.setVibrate(new long[] { 1000, 1000,1000,1000,1000,1000});
        boolean lollipop = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        if (lollipop) {

            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle(title)
                    .setStyle(
                            new NotificationCompat.BigTextStyle()
                                    .bigText(body))
                    .setContentText(body)
                    .setColor(Color.TRANSPARENT)
                    .setLargeIcon(
                            BitmapFactory.decodeResource(
                                    getResources(),
                                    R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher)

                    .setWhen(when).setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

        } else {

            mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setStyle(
                            new NotificationCompat.BigTextStyle()
                                    .bigText(body))
                    .setContentTitle(title).setContentText(body)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setWhen(when).setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

        }


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(new Random().nextInt(100) /* ID of notification */, mNotifyBuilder.build());
    }
}